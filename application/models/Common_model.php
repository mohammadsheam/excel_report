<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @package product
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */
class Common_model extends CI_Model {

    /**
     * this method use for all data retrive with table name
     * @author Jahid All Mamun
     */
    public function getall($table) {
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function all_customer($table) {
        $this->db->from("$table")->order_by('company_name');

        return $this->db->get()->result();
    }

    public function all_employee($table) {
        $this->db->from("$table")->order_by('name');

        return $this->db->get()->result();
    }

    public function getallData($table) {
        $this->db->where("deleted_at", NULL);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function getall_doctor($table) {
        $this->db->from("$table")->where('type', 7);
        return $this->db->get()->result();
    }

    /**
     * This function use for get all data with check status = 1
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $table
     * @return type
     */
    public function getall_with_status($table) {
        $this->db->where('status', 0);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function getwhere($table, $condition) {
        $text = "select *from " . $table . " where " . $condition;
        $query = $this->db->query("$text");
        $result = $query->result();
        return $result;
    }

    /**
     * this method use for one data or rows retrive with table name and id
     * @author Jahid All Mamun
     */
    public function getone($table, $id) {
        #Moinul	08/01/2020 4:15 pm
        ////////////////////////////////////////////////////////////////////
        //Get access permission to modules.
        $this->db->select($table . '.*, group_role.module');
        ////////////////////////////////////////////////////////////////////

        $this->db->where('users.id', $id);
        $this->db->limit(1);
        $this->db->from("$table");

        #Moinul	08/01/2020 3:29 pm
        ////////////////////////////////////////////////////////////////////
        //Get access permission to modules.
        $this->db->join('group_role', $table . '.type = group_role.group_id', 'left');
        ////////////////////////////////////////////////////////////////////

        return $this->db->get()->result();
    }

    public function getrow($table, $id) {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->row();
    }

    public function group_check($table, $id) {
        $this->db->where('group_id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->row();
    }

    public function group_role_details($id) {
        $this->db->where('group_id', $id);
        $this->db->limit(1);
        $this->db->from("group_role");
        return $this->db->get()->row();
    }

    public function customer_check($cus_email) {
        $this->db->select('customers.id as cusid, customer_shipping_address.id as shipid');
        $this->db->where('email', $cus_email);
        //$this->db->limit(1);
        $this->db->from("customers");
        $this->db->join('customer_shipping_address', 'customer_shipping_address.customer_id = customers.id');
        return $this->db->get()->row();
    }

    public function customer_shipping_info($table, $id) {
        $this->db->where('shipping_id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * this method use for one data or rows retrive with table name and id and status = 1
     * @author Jahid All Mamun
     */
    public function getone_with_status($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * delete row with table name and id
     * @author Jahid All Mamun
     */
    public function delete($table, $id, $primary = Null) {
        if ($primary != NULL) {
            $this->db->where($primary, $id);
        } else {
            $this->db->where('id', $id);
        }

        $resul = $this->db->delete($table);
        if ($resul) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function softdelete($table, $id, $primary = Null) {

        $data = array('deleted_at' => date('Y-m-d H:i:s'));
        if ($primary != NULL) {
            $this->db->where($primary, $id);
        } else {
            $this->db->where('id', $id);
        }

        $resul = $this->db->update($table, $data);
        if ($resul) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete_order_items($order_id) {
        $this->db->where('order_id', $id);
        $this->db->delete('paypal_order_items');
    }

    /**
     * This method use for save single row with table name and id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @param array value save with table name provided
     */
    public function save($table, $data) {
        $result = $this->db->insert($table, $data);
        if ($result) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function update($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //########### update for default ##################

    function update_default($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // #########  update shipp default #############
    function update_default_ship($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_page($table, $id, $lng_id, $data) {
        $this->db->where('id', $id);
        $this->db->where('lng_id', $lng_id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     *
     * @param type $id
     * @param type $data
     * @return boolean
     * this method use for active customer registration
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     */
    function active_acount($id, $data) {
        $this->db->where('password', $id);
        $result = $this->db->update('customers', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function updateall($table, $data) {
//        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * get all by id
     * @author jahid al mamun
     */
    public function getallbyid($table, $id) {
        $this->db->where('id', $id);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function get_all_with_custome_id($table, $con, $id) {
        $this->db->where($con, $id);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function getallbycolumn($table, $column, $value) {
        $this->db->where($column, $value);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    public function all_country() {
        $this->db->select('*');
        $this->db->order_by('country_shortName', 'asc');
        $this->db->from('country');
        return $this->db->get()->result();
    }

    /**
     *
     * This method use for get all data with id with status = 1
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     * @param type $table
     * @param type $id
     * @return type
     */
    public function getallbyid_with_status($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * check language exist with id and language id
     * @author Jahid Al Mamun
     * @param bool
     */
    public function lang_exist($table, $id, $lng_id) {
        $status = $this->db->where('id', $id)
                        ->where('lng_id', $lng_id)
                        ->from($table)
                        ->get()->row();
        if (!empty($status)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function blank_replace_all($text) {
        $text = strtolower(htmlentities($text));
        $text = str_replace(get_html_translation_table(), "_", $text);
        $text = str_replace(" ", "_", $text);
        $text = preg_replace("/[-]+/i", "_", $text);
        return $text;
    }

    /**
     * this method retrive region data with country id
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     * @return type
     *
     */
    public function get_region($country_id) {
        $query = $this->db->query("select *from region where country_id = $country_id order by name asc");
        return $query->result();
    }

    /**
     * This method retrive city data with country id
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     * @return type
     */
    public function get_city($country_id) {
        $query = $this->db->query("select *from city where country_id = $country_id order by name asc");
        return $query->result();
    }

    /**
     *
     * @param type $region_id
     * @return type
     * @author jahid al mamun
     */
    public function get_region_city($region_id) {
        $query = $this->db->query("select *from city where region_id = $region_id order by name asc");
        return $query->result();
    }

    /**
     * This method use for check valid email or not for different purpose as like forget password reset
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @param type $table
     * @param type $email
     * @return type
     */
    public function valid_email($table, $email) {
        $this->db->where('email', $email);
        $this->db->from($table);
        return $this->db->get()->result();
    }

    /**
     * Duplicate of above function to replace it in forget password
     * @author Ashikur Rahman
     * @param type $table
     * @param type $email
     * @return type
     */
    public function valid_email_1($table, $email) {
        $this->db->where('email', $email);
        $this->db->from($table);
        return $this->db->get();
    }

    /**
     *
     * @param type $id
     * @return type
     * this method use to get child menu in category left menu and call from category tree menu
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function child($id) {
        $this->table = 'category';
        $this->db->where('parent_id', $id);
        $this->db->from($this->table);
        return $child = $this->db->get()->result();
    }

    /**
     *
     * @param type $code
     * @return type
     * Tthis method use for get coupon information with coupon code
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function coupon_info($code) {
        $this->db->select('*');
        $this->db->where('code', $code);
        $this->db->from('coupon');
        $coupon = $this->db->get()->result();
        return $coupon;
    }

    /**
     *
     * @return type
     * This method use for get top category meny root
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function top_category_menu() {
        $this->db->select('id,name');
        $this->db->limit(3);
        $this->db->where('top', 1);
        $this->db->from('category');
        return $this->db->get()->result();
    }

    /**
     *
     * @return type
     * this method use to get email setting information from setting
     * @author jahid al mamu <rjs.jahid11@gmail.com>
     */
    public function email_setting() {
        $this->db->select('*');
        $this->db->from('email_setting');
        return $this->db->get()->result();
    }

    //retrun site email
    public function site_email() {
        $this->db->select('site_email');
        $this->db->from('site_setting');
        $result = $this->db->get()->result();
        $email = $result[0]->site_email;
        return $email;
    }

    /**
     *
     * @param type $id
     * @param type $lng_id
     * @return type
     * this method use to get page information
     * @author jahid al mamun <rjs.jahid11@gmal.com>
     */
    public function page_info($id, $lng_id) {
        $this->db->select('*');
        $this->db->join('page_lng', "page_lng.id=page.id and page_lng.lng_id = $lng_id", 'left');
        $this->db->where('page.id', $id);
        $this->db->from('page');
        return $this->db->get()->result();
    }

//######### get order items based on order id #############
    public function get_item_val($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from("order_items");
        return $this->db->get()->row();
    }

    public function get_order_val($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('orders')->row();
    }

// ########### then update specific order items discount and price ########
    public function item_price_update($data, $orId, $id) {
        $this->db->where('order_id', $orId);
        $this->db->where('id', $id);
        $this->db->update('order_items', $data);
        return $this->db->affected_rows();
    }

    public function order_price_update($data, $orId) {
        $this->db->where('id', $orId);
        $this->db->update('orders', $data);
        return $this->db->affected_rows();
    }

    //######### get order items based on order id #############
    public function get_items($id) {
        $this->db->select('*');
        $this->db->where('order_id', $id);
        return $this->db->get('order_items')->result();
    }

    public function get_by_id($id, $table) {
        $this->db->from($table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function doctor_schedule($id) {
        $this->db->select('*');
        $this->db->from('doctor_shedule');
        $this->db->where('doctor_id', $id);
        $query = $this->db->get();

        return $query->result();
    }


	public function get_employees_by_date($department,$fromdate,$todate){
		$fromdate = date('Y-m-d', strtotime($fromdate));
		$todate = date('Y-m-d', strtotime($todate));
		//$department = date('Y-m-d', strtotime($department));
		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name');
        $this->db->join('service_engineer','service_engineer.id = assign_jobs.se_id','left');
        $this->db->join('jobs','jobs.id = assign_jobs.job_id','left');
		$this->db->where('DATE(assign_jobs.ap_from_ts) >=',$fromdate);
        $this->db->where('DATE(assign_jobs.ap_from_ts) <=',$todate);
        $this->db->where('service_engineer.department',$department);
        $this->db->order_by('service_engineer.name','asc');
		$this->db->group_by("service_engineer.id");
		$this->db->from('assign_jobs');
		$query = $this->db->get();

		$resultArray = array();
        $resultArray['result'] = $query->result();
        return $resultArray;
    }

    /*public function get_employees_by_date($curr_date){
		$curr_date = date('Y-m-d', strtotime($curr_date));
		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name');
		$this->db->join('service_engineer','service_engineer.id = assign_jobs.se_id','left');
		$this->db->where('DATE(ap_from_ts)',$curr_date);
		$this->db->group_by("service_engineer.id");
		$this->db->from('assign_jobs');
		$query = $this->db->get();
		$totalCount = $query->num_rows();

		$resultArray = array();
		$resultArray['result'] = $query->result();
		return $resultArray;
	}*/

	// Query method to get daily job details
	public function get_daily_data($curr_date, $offset, $limit, $department, $requiredtotal, $sort, $userIds){
        $curr_date = date('Y-m-d',strtotime($curr_date));

        if($requiredtotal){
			$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,assign_jobs.ap_from_ts as assign_date,service_engineer.department as emp_department')
			->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
			->join('jobs','jobs.id = assign_jobs.job_id','left')
            ->where('DATE(ap_from_ts)',$curr_date);
            if(!empty($department)){
				$this->db->where('service_engineer.department',$department);
			}
            if(!empty($userIds)){
                $this->db->where_in('service_engineer.id', $userIds);
            }

			$this->db->group_by("service_engineer.id");
			$this->db->order_by('service_engineer.name','asc');
			$query = $this->db->get('assign_jobs');
			$totalResults = $query->num_rows();
			$returnArray['totalResults'] = $totalResults;
		}

		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,assign_jobs.ap_from_ts as assign_date,service_engineer.department as emp_department')
		->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
		->join('jobs','jobs.id = assign_jobs.job_id','left')
        ->where('DATE(ap_from_ts)',$curr_date);
        if(!empty($department)){
            $this->db->where('service_engineer.department',$department);
        }

        if(!empty($userIds)){
            $this->db->where_in('service_engineer.id', $userIds);
        }

		if (isset($sort)) {
			$sort = json_decode($sort, 1);
			if (count($sort)) {
				$order = $sort[0]['desc'] ? 'DESC' : 'ASC';
				$selector = $sort[0]['selector'];
				$orderSelector = '';
				switch($selector){
					case 'emp_name':
					$orderSelector = 'service_engineer.name';
					break;
					case 'emp_department':
					$orderSelector = 'service_engineer.department';
					break;
					default:
					$orderSelector = 'service_engineer.name';
					$order = 'asc';
				}

				if($selector == 'jobcount'){
					$orderSelector = 'service_engineer.name';
				}
				$this->db->order_by($orderSelector, $order);
			}
		}

		$this->db->group_by("service_engineer.id");
		$this->db->order_by('service_engineer.name','asc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get('assign_jobs');
		$result = $query->result();

		foreach($result as $key => $value){

			$this->db->select('jobs.job_no as ticket_no, assign_jobs.status as job_status, job_maintenance_type.name as job_type');
			$this->db->join('service_engineer','service_engineer.id = assign_jobs.se_id','left');
			$this->db->join('jobs','jobs.id = assign_jobs.job_id','left');
			$this->db->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left');
			$this->db->where('DATE(ap_from_ts)',$curr_date);
			$this->db->where('service_engineer.id',$value->emp_id);
			$query = $this->db->get('assign_jobs');
			$job_info_result = $query->result();
			$j = 1;
			foreach($job_info_result as $k=>$v){
				$jobKey = 'job_'.$j;
				$result[$key]->$jobKey = $job_info_result[$k];
				$j++;
			}

		}
		$returnArray['result'] = $result;

		return $returnArray;
	}

	public function get_weekly_data($startdate,$enddate,$offset,$limit, $department, $requiredtotal, $sort){
		$curr_date = date('Y-m-d',strtotime($startdate));
		$range_date = date('Y-m-d', strtotime($enddate));

		if($requiredtotal){
			$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,jobs.id as job_id,service_engineer.department as emp_department, COUNT(service_engineer.id) AS jobcount')
			->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
			->join('jobs','jobs.id = assign_jobs.job_id','left')
			->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left')
			->where('DATE(ap_from_ts) >=',$curr_date)
			->where('DATE(ap_from_ts) <=',$range_date);
			if(!empty($department)){
				$this->db->where('department',$department);
			}

			$this->db->group_by("service_engineer.id");
			$this->db->order_by('service_engineer.name','asc');
			$query = $this->db->get('assign_jobs');
			$totalResults = $query->num_rows();
			$returnArray['totalResults'] = $totalResults;
		}

		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,jobs.id as job_id,service_engineer.department as emp_department, COUNT(service_engineer.id) AS jobcount')
		->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
		->join('jobs','jobs.id = assign_jobs.job_id','left')
		->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left')
		->where('DATE(ap_from_ts) >=',$curr_date)
		->where('DATE(ap_from_ts) <=',$range_date);
		if(!empty($department)){
			$this->db->where('department',$department);
		}
		if (isset($sort)) {
			$sort = json_decode($sort, 1);
			if (count($sort)) {
				$order = $sort[0]['desc'] ? 'DESC' : 'ASC';
				$selector = $sort[0]['selector'];
				$orderSelector = '';
				switch($selector){
					case 'emp_name':
					$orderSelector = 'service_engineer.name';
					break;
					case 'emp_department':
					$orderSelector = 'service_engineer.department';
					break;
					default:
					$orderSelector = 'service_engineer.name';
					$order = 'asc';
				}

				if($selector == 'jobcount'){
					$orderSelector = 'service_engineer.name';
				}
				$this->db->order_by($orderSelector, $order);
			}
		}
		$this->db->group_by("service_engineer.id");
		$this->db->limit($limit,$offset);
		$result = $this->db->get('assign_jobs');
		$resultArray = $result->result();

		foreach($resultArray as $key => $value){
			$emp_id = $value->emp_id;

			/* $this->db->select('assign_jobs.ap_from_ts as assign_date, COUNT(service_engineer.id) AS jobs_assigned'); */
			$this->db->select('DATE_FORMAT(assign_jobs.ap_from_ts,"%d-%m-%Y") as assign_date, COUNT(service_engineer.id) AS jobs_assigned');
			$this->db->join('service_engineer','service_engineer.id = assign_jobs.se_id','left');
			$this->db->join('jobs','jobs.id = assign_jobs.job_id','left');
			$this->db->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left');

			$this->db->where('DATE(ap_from_ts) >=',$curr_date);
			$this->db->where('DATE(ap_from_ts) <=',$range_date);
			$this->db->where('service_engineer.id',$emp_id);
			$this->db->group_by("assign_jobs.ap_from_ts");
			$query = $this->db->get('assign_jobs');
			$jobCountResult = $query->result();

			foreach($jobCountResult as $k => $v){
				$dateKey = date('Ymd',strtotime($v->assign_date));
				$jobsAssigned = $v->jobs_assigned;
				if(isset($resultArray[$key]->$dateKey)){
					$jobsAssigned += $resultArray[$key]->$dateKey;
				}
				$resultArray[$key]->$dateKey  = $jobsAssigned;
			}
		}

		$returnArray['result'] = $resultArray;

		return $returnArray;
	}

	//Query Method to fetch employee's jobs
	public function get_employee_job_detail($emp_id, $startDate, $endDate, $offset, $limit, $sort, $required_total, $type){

        $startDate = date('Y-m-d',strtotime($startDate));
		if(!empty($endDate)){
			$endDate = date('Y-m-d',strtotime($endDate));
		}
		if($required_total){
			$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,jobs.id as job_id,service_engineer.department as emp_department, job_maintenance_type.name as job_type')
			->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
			->join('jobs','jobs.id = assign_jobs.job_id','left')
			->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left');
			if($type == 'weekly'){
				$this->db->where('DATE(ap_from_ts) >=',$startDate);
				$this->db->where('DATE(ap_from_ts) <=',$endDate);
			} else {
				$this->db->where('DATE(ap_from_ts)',$startDate);
			}

			$this->db->where('service_engineer.id',$emp_id);
			$this->db->order_by('service_engineer.name','asc');
			$query = $this->db->get('assign_jobs');
			$totalResults =  $query->num_rows();
			$returnArray['totalResults'] = $totalResults;
		}


		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,jobs.id as job_id,service_engineer.department as emp_department, assign_jobs.ap_from_ts as assign_date, jobs.job_no as ticket_no, assign_jobs.status as job_status, job_maintenance_type.name as job_type')
		->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
		->join('jobs','jobs.id = assign_jobs.job_id','left')
		->join('job_maintenance_type','job_maintenance_type.id = jobs.maintenance_type','left');

		if($type == 'weekly'){
			$this->db->where('DATE(ap_from_ts) >=',$startDate);
			$this->db->where('DATE(ap_from_ts) <=',$endDate);
		} else {
			$this->db->where('DATE(ap_from_ts)',$startDate);
		}

		$this->db->where('service_engineer.id',$emp_id);
		if (isset($sort)) {
			$sort = json_decode($sort, 1);
			if (count($sort)) {
				$order = $sort[0]['desc'] ? 'DESC' : 'ASC';
				$selector = $sort[0]['selector'];
				$orderSelector = '';
				switch($selector){
					case 'job_id':
					$orderSelector = 'jobs.id';
					break;
					case 'job_type':
					$orderSelector = 'job_maintenance_type.name';
					break;
					case 'job_status':
					$orderSelector = 'assign_jobs.status';
					break;
					case 'ticket_no':
					$orderSelector = 'jobs.job_no';
					break;
					case 'emp_department':
					$orderSelector = 'service_engineer.department';
					break;
					case 'assign_date':
					$orderSelector = 'assign_jobs.ap_from_ts';
					break;
					default:
					$orderSelector = 'service_engineer.name';
					$order = 'asc';
				}

				if($selector == 'jobcount'){
					$orderSelector = 'service_engineer.name';
				}
				$this->db->order_by($orderSelector, $order);
			}
		}

		$this->db->limit($limit,$offset);
		$query = $this->db->get('assign_jobs');
		$result = $query->result();



		$returnArray['result'] = $result;
		return $returnArray;
	}

	//Query Method to get job details by job id
	public function job_details($id, $action){
        $query = 'jobs.*,job_maintenance_type.name as maintenance_type,job_priority_type.name as priority,job_urgency_type.name as urgency, customer.company_name, customer_contract.contract_no, contact_type.name as contract_type, system.system_name, assign_jobs.status, support_type.name as support_type, customer_contract.service_frequency,  CONCAT_WS(" ", customer_contract.address1, customer_contract.address2, customer_contract.postcode) AS site_address';
        if($action != ''){
            $query .= ', job_tearm_question.*';
        }
		$this->db->select($query);
		$this->db->from('jobs');
		$this->db->where('jobs.id', $id);

		$this->db->join('customer', 'customer.id = jobs.customer_id', 'left');
		$this->db->join('customer_contract', 'customer_contract.id = jobs.contract_id', 'left');

        $this->db->join('assign_jobs','assign_jobs.job_id = jobs.id','left');

        $this->db->join('contact_type','contact_type.id = customer_contract.contract_type','left');
        $this->db->join('support_type','support_type.id = customer_contract.support_type','left');

		$this->db->join('job_maintenance_type', 'job_maintenance_type.id = jobs.maintenance_type', 'left');
		$this->db->join('job_priority_type', 'job_priority_type.id = jobs.priority', 'left');
		$this->db->join('job_urgency_type', 'job_urgency_type.id = jobs.urgency', 'left');
        $this->db->join('system', 'system.id = jobs.system_id', 'left');
        if($action != ''){
            $this->db->join('job_tearm_question', 'job_tearm_question.seid = assign_jobs.se_id AND job_tearm_question.job_assign_id = assign_jobs.id', 'left');
        }


        $query = $this->db->get();
        $data = array();
        $data = $query->result()    ;
		foreach($query->result() as $rows)
		{
			/*$nestedData['job_no'] = $rows->job_no;
			$nestedData['caller_name'] = $rows->caller_name;
			$nestedData['caller_contact_no'] = $rows->caller_name;
			$nestedData['issue_title'] = $rows->issue_title;
			$nestedData['customer_id'] = $rows->customer_id;
			$nestedData['contract_no'] = $rows->contract_no;
			$nestedData['contract_id'] = $rows->contract_id;
			$nestedData['issue_description'] = $rows->issue_description;
			$nestedData['urgency'] = $rows->urgency;
			$nestedData['priority'] = $rows->priority;
			$nestedData['received_by'] = $rows->received_by;
			$nestedData['maintenance_type'] = $rows->maintenance_type;
			$nestedData['company_name'] = $rows->company_name;
			$nestedData['contract_type'] = $rows->contract_type;
			$nestedData['priority'] = $rows->priority;
			$nestedData['id'] = $rows->id;
			$nestedData['urgency'] = $rows->urgency;
			$nestedData['system_name'] = $rows->system_name;
			$nestedData['job_status'] = $rows->status;
			$nestedData['support_type'] = $rows->support_type;
			$nestedData['service_frequency'] = $rows->service_frequency;
            $nestedData['site_address'] = $rows->site_address;

            $nestedData['q1'] = $rows->q1;
            $nestedData['r1'] = $rows->r1;
            $nestedData['site_address'] = $rows->site_address;
            $nestedData['site_address'] = $rows->site_address;
            $nestedData['site_address'] = $rows->site_address;






            [q1] => 0
            [r1] =>
            [q2] => 0
            [r2] =>
            [q3] => 0
            [r3] =>
            [q4] => 0
            [r4] =>
            [q5] => 0
            [r5] =>
            [q6] => 0
            [r6] =>
            [q7] => 0
            [r7] =>
            [q8] => 0
            [r8] =>
            [q9] => 0
            [r9] =>
            [q10] => 0
            [r10] =>
            [q11] => 0
            [r11] =>
            [q12] => 0
            [r12] =>
            [q13] => 0
            [r13] =>
            [q14] => 1
            [r14] =>
            [q15] => 1
            [r15] =>

			$data = $nestedData;*/

		}
		$returnArray['result'] = $data;
		return $returnArray;
	}

	// Query method for get employee total working hour with specific job and date
	public function employee_working_hour($emp_id,$date,$job_id){
		$sql = "SELECT job_checkin_checkout.start_time as 'From', job_checkin_checkout.end_time AS 'To',job_checkin_checkout.hrs_count as total_hours, job_checkin_checkout.overtime AS 'Overtime' FROM `job_checkin_checkout` WHERE se_id = ".$engineer_id." AND manual_date = '".$work_date."' AND job_id =".$job_id;
		$query = $this->db->query ($sql);
		return $query->result();
    }

   //Query method to get details for weekly report graph
	public function get_weekly_chart_data($startdate,$enddate,$offset,$limit, $department, $requiredtotal, $sort, $se_ids){
		$curr_date = date('Y-m-d',strtotime(str_replace('/','-',$startdate)));
		$range_date = date('Y-m-d', strtotime(str_replace('/','-',$enddate)));


		$sql = ' SELECT

					SUM(CASE WHEN ( assign_jobs.status = 0 ) THEN 1 ELSE 0 END) as notstarted,
					SUM(CASE WHEN assign_jobs.status = 2 THEN 1 ELSE 0 END) as done,
					SUM(CASE WHEN assign_jobs.status = 3 THEN 1 ELSE 0 END) as inprogress,
					SUM(
					CASE WHEN (assign_jobs.status = 3 AND date(ap_from_ts) != date(assign_jobs.job_start_datetime))
					 THEN 1 ELSE 0 END) as latestarted,
					date(ap_from_ts) as date
					FROM assign_jobs, service_engineer
					WHERE assign_jobs.se_id = service_engineer.id
							AND date(ap_from_ts) BETWEEN "'.$curr_date.'" AND "'.$range_date.'" ';

		if(!empty($department)){
			$sql .=' AND service_engineer.department = "'.strtolower($department).'" ';
        }

		 if(!empty($se_ids) && is_array($se_ids) && count($se_ids)){
			if(count($se_ids) == 1){
				$sql .= " AND service_engineer.id = ".$se_ids[0];
			}else{
				$sql .= " AND service_engineer.id IN ( ".implode(', ',$se_ids)." ) ";
			}
		}

		$sql .=' GROUP BY DATE(ap_from_ts) order by date(ap_from_ts) ASC ';
		$query  = $this->db->query($sql);
		$result = $query->result_array();
		foreach($result as $key => $val){
			$result[$key]['notstarted'] = (int) $result[$key]['notstarted'];
			$result[$key]['done'] = (int) $result[$key]['done'];
			$result[$key]['inprogress'] = (int) $result[$key]['inprogress'];
			$result[$key]['latestarted'] = (int) $result[$key]['latestarted'];
			$result[$key]['date'] =  date('d-M',strtotime($result[$key]['date']));
			$result[$key]['date_real'] =  date('Y-m-d',strtotime($result[$key]['date']));
		}
		//echo "<pre>"; print_r($query); die();

		/*$this->db->select('DATE_FORMAT(assign_jobs.ap_from_ts,"%d-%m-%Y") as assign_date, DATE_FORMAT(assign_jobs.ap_from_ts,"%d/%a ") as day')
		->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
		->where('DATE(ap_from_ts) >=',$curr_date)
		->where('DATE(ap_from_ts) <=',$range_date);
		if(!empty($department)){
			$this->db->where('department',$department);
        }
        if(!empty($se_ids) && is_array($se_ids)){
           $this->db->where_in('service_engineer.id', $se_ids);
		}
        $this->db->group_by('CAST(assign_jobs.ap_from_ts as date)');
		$result = $this->db->get('assign_jobs');
        $resultArray = $result->result();
        //print_r($resultArray); die;

		foreach($resultArray as $key => $value){
            $resultArray[$key]->done = 0;
            $resultArray[$key]->notstarted = 0;
            $resultArray[$key]->inprogress = 0;
            $resultArray[$key]->latestarted = 0;

            $this->db->select('assign_jobs.status, DATE_FORMAT(assign_jobs.ap_from_ts,"%d-%m-%Y") as start_date, DATE_FORMAT(assign_jobs.created_ts,"%d-%m-%Y") as assigned_date')
            ->join('service_engineer','service_engineer.id = assign_jobs.se_id','left');
            $this->db->where('DATE(assign_jobs.ap_from_ts) LIKE ',date('Y-m-d', strtotime($value->assign_date)));
            if(!empty($department)){
                $this->db->where('department',$department);
            }
            $query = $this->db->get('assign_jobs');
            $jobCountResult = $query->result();

            $today = date('Y-m-d');

            foreach($jobCountResult as $k => $v){
                $status = $v->status;
                $assign_date = $v->assigned_date;
                $job_start_date = $v->start_date;

                $currentTime=strtotime($today);
                // Reset time to 00:00:00
                $assignd_date_timestamp=strtotime(date('Y-m-d 00:00:00',strtotime($assign_date)));
                $start_date_timestamp=strtotime(date('Y-m-d 00:00:00',strtotime($job_start_date)));
                $days=round(($assignd_date_timestamp-$currentTime)/86400);
                if($days < 0 && $status == 0){
                    $status = 0;
                }

                switch($status){
                    case 0:
                        //Not started yet
                        $resultArray[$key]->notstarted = $resultArray[$key]->notstarted+1;
                        break;
                    case 1:
                        //  Pending Status means not started yet
                        $resultArray[$key]->notstarted = $resultArray[$key]->notstarted+1;
                        break;
                    case 2:
                        // Job Done
                        $resultArray[$key]->done = $resultArray[$key]->done + 1;
                        break;
                    case 3:
                        if($assignd_date_timestamp == $start_date_timestamp){
                            // Job Started on time and working
                            $resultArray[$key]->inprogress = $resultArray[$key]->inprogress+1;
                        } else {
                            // Job late started and stillworking
                            $resultArray[$key]->latestarted = $resultArray[$key]->latestarted+1;
                        }
                        break;
                }
            }
		}*/

        $returnArray['result'] = $result;

		return $returnArray;
    }



    public function get_ehs_data($startdate,$enddate,$offset,$limit, $department, $requiredtotal, $sort){
		$curr_date = date('Y-m-d',strtotime($startdate));
		$range_date = date('Y-m-d', strtotime($enddate));

		if($requiredtotal){
			$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name,jobs.id as job_id,service_engineer.department as emp_department, COUNT(service_engineer.id) AS jobcount')
			->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
            ->join('jobs','jobs.id = assign_jobs.job_id','left')

            ->join('job_tearm_question', 'job_tearm_question.seid = assign_jobs.se_id AND job_tearm_question.job_assign_id = assign_jobs.id', 'left')

			->where('DATE(assign_jobs.ap_from_ts) >=',$curr_date)
			->where('DATE(assign_jobs.ap_from_ts) <=',$range_date)
                ->where('assign_jobs.job_id IS NOT NULL')
			->where('assign_jobs.status',3);
			if(!empty($department)){
				$this->db->where('service_engineer.department',$department);
			}

			$this->db->order_by('service_engineer.name','asc');
			$query = $this->db->get('assign_jobs');
			$totalResults = $query->num_rows();
			$returnArray['totalResults'] = $totalResults;

        }


		$this->db->select('service_engineer.id as emp_id, service_engineer.name as emp_name, jobs.id as job_id, assign_jobs.ap_from_ts, assign_jobs.status, service_engineer.department, jobs.job_no as ticket_no, job_tearm_question.*')
		->join('service_engineer','service_engineer.id = assign_jobs.se_id','left')
        ->join('jobs','jobs.id = assign_jobs.job_id','left')

        ->join('job_tearm_question', 'job_tearm_question.seid = assign_jobs.se_id AND job_tearm_question.job_assign_id = assign_jobs.id', 'left')

        ->where('DATE(assign_jobs.ap_from_ts) >=',$curr_date)
        ->where('DATE(assign_jobs.ap_from_ts) <=',$range_date)
        ->where('assign_jobs.job_id IS NOT NULL ')
        ->where('assign_jobs.status ', 2);
		if(!empty($department)){
			$this->db->where('service_engineer.department',$department);
        }

		if (isset($sort)) {
			$sort = json_decode($sort, 1);
			if (count($sort)) {
				$order = $sort[0]['desc'] ? 'DESC' : 'ASC';
				$selector = $sort[0]['selector'];
				$orderSelector = 'jobs.job_no';

				$this->db->order_by($orderSelector, $order);
			}
		}

        $this->db->limit($limit,$offset);
		$result = $this->db->get('assign_jobs');
        $resultArray = $result->result();
        //$this->output->enable_profiler(TRUE);
        $returnArray['result'] = $resultArray;

		return $returnArray;
	}

	function getDailyReportData(){


	}
  public function get_feedback_data($startdate,$enddate,$offset,$limit, $department, $requiredtotal, $sort, $se_ids){
  		$curr_date = date('Y-m-d',strtotime(str_replace('/','-',$startdate)));
  		$range_date = date('Y-m-d', strtotime(str_replace('/','-',$enddate)));
  		$sql = ' SELECT
  					SUM(CASE WHEN ( assign_jobs.status = 0 ) THEN 1 ELSE 0 END) as notstarted,
  					SUM(CASE WHEN assign_jobs.status = 2 THEN 1 ELSE 0 END) as done,
  					SUM(CASE WHEN assign_jobs.status = 3 THEN 1 ELSE 0 END) as inprogress,
  					SUM(
  					CASE WHEN (assign_jobs.status = 3 AND date(ap_from_ts) != date(assign_jobs.job_start_datetime))
  					 THEN 1 ELSE 0 END) as latestarted,
  					date(ap_from_ts) as date
  					FROM assign_jobs, service_engineer
  					WHERE assign_jobs.se_id = service_engineer.id
  							AND date(ap_from_ts) BETWEEN "'.$curr_date.'" AND "'.$range_date.'" ';

  		if(!empty($department)){
  			$sql .=' AND service_engineer.department = "'.strtolower($department).'" ';
          }

  		 if(!empty($se_ids) && is_array($se_ids) && count($se_ids)){
  			if(count($se_ids) == 1){
  				$sql .= " AND service_engineer.id = ".$se_ids[0];
  			}else{
  				$sql .= " AND service_engineer.id IN ( ".implode(', ',$se_ids)." ) ";
  			}
  		}
  		$sql .=' GROUP BY DATE(ap_from_ts) order by date(ap_from_ts) ASC ';
  		$query  = $this->db->query($sql);
  		$result = $query->result_array();
  		foreach($result as $key => $val){
  			$result[$key]['notstarted'] = (int) $result[$key]['notstarted'];
  			$result[$key]['done'] = (int) $result[$key]['done'];
  			$result[$key]['inprogress'] = (int) $result[$key]['inprogress'];
  			$result[$key]['latestarted'] = (int) $result[$key]['latestarted'];
  			$result[$key]['date'] =  date('d-M',strtotime($result[$key]['date']));
  			$result[$key]['date_real'] =  date('Y-m-d',strtotime($result[$key]['date']));
  		}
      $returnArray['result'] = $result;
  		return $returnArray;
    }

    public function get_feedback_report($startdate,$enddate,$department,$limit,$currPage){
      if(!empty($startdate) && !empty($enddate)){
          $curr_date  = date('Y-m-d',strtotime($startdate));
          $range_date = date('Y-m-d',strtotime($enddate));
      }
  		$query  = 'SELECT `FB`.*,`JB`.`job_no`,`JB`.`datetime` AS `job_datetime`,`AJ`.`cust_name` AS `contact_name`,`CS`.`company_name`,`CO`.`address1`,`MT`.`name` AS `mt_type`,`SE`.`name` AS `eng_name` ';
      $query .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
      $query .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
      $query .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
      if(!empty($startdate) && !empty($enddate)){
          $query .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
      }
      // ( DATE(`FB`.`datetime`) BETWEEN "'.$curr_date.'" AND "'.$range_date.'") AND ';
  		if(!empty($department) && $department != 'All'){
  			$query .=' AND `SE`.`department` = "'.strtolower($department).'" ';
      }
      //$query .= 'GROUP BY `FB`.`assign_id` ';
      if(!empty($currPage) && $currPage > 0){
          $offset = ($limit * $currPage) - $limit;
          $startRow = $offset + 1; $endRow = $offset + $limit;
          $query .= 'ORDER BY `FB`.`datetime` DESC LIMIT '.$offset.','.$limit;
      }else{
          $startRow = 1; $endRow = $limit;
          $query .= 'ORDER BY `FB`.`datetime` DESC LIMIT '.$limit;
      }

  		$retrive = $this->db->query($query);
  		$result  = $retrive->result_array();
      //--Paginition Info-------------
      $query  = 'SELECT `FB`.*,`JB`.`job_no`,`JB`.`datetime` AS `job_datetime`,`AJ`.`cust_name` AS `contact_name`,`CS`.`company_name`,`CO`.`address1`,`MT`.`name` AS `mt_type`,`SE`.`name` AS `eng_name` ';
      $query .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
      $query .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
      $query .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
      if(!empty($startdate) && !empty($enddate)){
          $query .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
      }
      // ( DATE(`FB`.`datetime`) BETWEEN "'.$curr_date.'" AND "'.$range_date.'") AND ';
  		if(!empty($department) && $department != 'All'){
  			$query .=' AND `SE`.`department` = "'.strtolower($department).'" ';
      }
      //$query .= 'GROUP BY `FB`.`assign_id` ';
      //$query .= 'ORDER BY `FB`.`datetime` DESC LIMIT '.$limit;
  		$retrive = $this->db->query($query);
      $returnArray['query']  = $query;
      $totalRows = $retrive->num_rows(); $lastPage = $totalRows / $limit;
      $extra = ($totalRows % $limit == 0)? 0: 1; $lastPage = intval($lastPage) + $extra;
      if(!empty($currPage) && $currPage > 0){
          $offset = ($limit * $currPage) - $limit;
          if($totalRows <= 0){
              $startRow = 0;
          }else{
              $startRow = ($totalRows > $offset) ? $offset + 1 : $offset - $limit;
          }
          $nextRow = $offset + $limit;
          if($totalRows < $nextRow){
              $endRow = $totalRows;
          }else{
              $endRow = $nextRow;
          }
      }else{
          $startRow = ($totalRows > 0) ? 1 : 0;
          $endRow   = ($totalRows > $limit) ? $limit : $totalRows;
      }
      $page['total'] = $totalRows;
      $page['lastPage'] = $lastPage;
      //$page['currView'] = $currentTotal;
      $page['startRow'] = $startRow;
      $page['endRow']   = $endRow;

      // Format for excel export
      $query  = 'SELECT `JB`.`job_no` AS `Job No.`,`FB`.`datetime` AS `Feedback DateTime`,`JB`.`datetime` AS `Job DateTime`,`AJ`.`cust_name` AS `CustomerName`,`CS`.`company_name` AS `Company Name`,`SE`.`name` AS `Engineer Name`,`CO`.`address1` AS `Address`,`MT`.`name` AS `MT Type`, ';
      $query .= '`FB`.`q1` AS `Quality-Mark`,`FB`.`r1` AS `Quality-Note`,`FB`.`q2` AS `Delivery-Mark`,`FB`.`r2` AS `Delivery-Note`,`FB`.`q3` AS `Relationship-Mark`,`FB`.`r3` AS `Relationship-Note`, ';
      $query .= '`FB`.`q4` AS `Responsiveness-Mark`,`FB`.`r4` AS `Responsiveness-Note`,`FB`.`q5` AS `Overall Satisfaction-Mark`,`FB`.`r5` AS `Overall Satisfaction-Note`, `FB`.`r6` AS `Final Complements`';
      $query .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
      $query .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
      $query .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
      if(!empty($startdate) && !empty($enddate)){
          $query .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
      }
      // ( DATE(`FB`.`datetime`) BETWEEN "'.$curr_date.'" AND "'.$range_date.'") AND ';
  		if(!empty($department) && $department != 'All'){
  			$query .=' AND `SE`.`department` = "'.strtolower($department).'" ';
      }
      //$query .= 'GROUP BY `FB`.`assign_id` ';
      $query .= 'ORDER BY `FB`.`datetime` DESC LIMIT '.$limit;

  		$retrive = $this->db->query($query);
  		$excelSheet = $retrive->result_array();
      // Mark 0
      $marks = array(0,1,2,3,4,5,6,7,8,9,10);
      $marks_array = []; $loop = 1;
      foreach($marks as $mark){
          //Quetion - 1
          $query1  = 'SELECT * ';
          $query1 .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
          $query1 .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
          $query1 .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
          if(!empty($startdate) && !empty($enddate)){
              $query1 .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
          }
          if(!empty($department) && $department != 'All'){
              $query1 .= 'AND `SE`.`department` = "'.strtolower($department).'" ';
          }
          $query1 .= 'AND `FB`.`q1` = "'.$mark.'" ';
          $retrivQ = $this->db->query($query1);
          $marks_array[$mark][] = $retrivQ->num_rows() <= 0 ? 0: $retrivQ->num_rows();
          //Quetion - 2
          $query2  = 'SELECT * ';
          $query2 .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
          $query2 .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
          $query2 .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
          if(!empty($startdate) && !empty($enddate)){
              $query2 .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
          }
          if(!empty($department) && $department != 'All'){
              $query2 .= 'AND `SE`.`department` = "'.strtolower($department).'" ';
          }
          $query2 .= 'AND `FB`.`q2` = "'.$mark.'" ';
          $retrivQ = $this->db->query($query2);
          $marks_array[$mark][] = $retrivQ->num_rows() <= 0 ? 0: $retrivQ->num_rows();
          //Quetion - 3
          $query3  = 'SELECT * ';
          $query3 .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
          $query3 .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
          $query3 .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
          if(!empty($startdate) && !empty($enddate)){
              $query3 .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
          }
          if(!empty($department) && $department != 'All'){
              $query3 .= 'AND `SE`.`department` = "'.strtolower($department).'" ';
          }
          $query3 .= 'AND `FB`.`q3` = "'.$mark.'" ';
          $retrivQ = $this->db->query($query3);
          $marks_array[$mark][] = $retrivQ->num_rows() <= 0 ? 0: $retrivQ->num_rows();
          //Quetion - 4
          $query4  = 'SELECT * ';
          $query4 .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
          $query4 .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
          $query4 .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
          if(!empty($startdate) && !empty($enddate)){
              $query4 .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
          }
          if(!empty($department) && $department != 'All'){
              $query4 .= 'AND `SE`.`department` = "'.strtolower($department).'" ';
          }
          $query4 .= 'AND `FB`.`q4` = "'.$mark.'" ';
          $retrivQ = $this->db->query($query4);
          $marks_array[$mark][] = $retrivQ->num_rows() <= 0 ? 0: $retrivQ->num_rows();
          //Quetion - 5
          $query5  = 'SELECT * ';
          $query5 .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
          $query5 .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
          $query5 .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
          if(!empty($startdate) && !empty($enddate)){
              $query5 .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
          }
          if(!empty($department) && $department != 'All'){
              $query5 .= 'AND `SE`.`department` = "'.strtolower($department).'" ';
          }
          $query5 .= 'AND `FB`.`q5` = "'.$mark.'" ';
          $retrivQ = $this->db->query($query5);
          $marks_array[$mark][] = $retrivQ->num_rows() <= 0 ? 0: $retrivQ->num_rows();
      }

      $returnArray['result'] = $result;
      $returnArray['questi'] = $marks_array;
      $returnArray['excel']  = $excelSheet;
      $returnArray['paginition'] = $page;
      return $returnArray;
    }
    public function get_feedback_report_all_excel(){
      // Format for excel export
      $query  = 'SELECT `JB`.`job_no` AS `Job No.`,`FB`.`datetime` AS `Feedback DateTime`,`JB`.`datetime` AS `Job DateTime`,`AJ`.`cust_name` AS `CustomerName`,`CS`.`company_name` AS `Company Name`,`SE`.`name` AS `Engineer Name`,`CO`.`address1` AS `Address`,`MT`.`name` AS `MT Type`, ';
      $query .= '`FB`.`q1` AS `Quality-Mark`,`FB`.`r1` AS `Quality-Note`,`FB`.`q2` AS `Delivery-Mark`,`FB`.`r2` AS `Delivery-Note`,`FB`.`q3` AS `Relationship-Mark`,`FB`.`r3` AS `Relationship-Note`, ';
      $query .= '`FB`.`q4` AS `Responsiveness-Mark`,`FB`.`r4` AS `Responsiveness-Note`,`FB`.`q5` AS `Overall Satisfaction-Mark`,`FB`.`r5` AS `Overall Satisfaction-Note`, `FB`.`r6` AS `Final Complements`';
      $query .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
      $query .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
      $query .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
  		$retrive = $this->db->query($query);
  		$excelSheet = $retrive->result_array();
      $returnArray['excel']  = $excelSheet;
      return $returnArray;
    }
    public function get_feedback_report_pdf($startdate,$enddate,$department){
      if(!empty($startdate) && !empty($enddate)){
          $curr_date  = date('Y-m-d',strtotime($startdate));
          $range_date = date('Y-m-d',strtotime($enddate));
      }
  		$query  = 'SELECT `FB`.*,`JB`.`job_no`,`JB`.`datetime` AS `job_datetime`,`AJ`.`cust_name` AS `contact_name`,`AJ`.`signature` AS `cus_signature`,`AJ`.`engineer_signature` AS `eng_signature`,`CS`.`company_name`,`CO`.`address1`,`MT`.`name` AS `mt_type`,`SE`.`name` AS `eng_name`, `JB`.`issue_title` AS `issue_title`, `JB`.`caller_contact_no` AS `phone_no` ';
      $query .= 'FROM `service_feedback` `FB`,`assign_jobs` `AJ`,`jobs` `JB`,`customer` `CS`,`customer_contract` `CO`,`job_maintenance_type` `MT`,`service_engineer` `SE` ';
      $query .= 'WHERE (`FB`.`assign_id` = `AJ`.`id` AND `AJ`.`job_id` = `JB`.`id` AND `JB`.`maintenance_type` = `MT`.`id` AND `JB`.`customer_id` = `CS`.`id` AND `JB`.`contract_id` = `CO`.`id` AND ';
      $query .= '`AJ`.`se_id` = `SE`.`id` AND (`FB`.`assign_id` != "0" AND `FB`.`assign_id` != 0)) ';
      if(!empty($startdate) && !empty($enddate)){
          $query .= 'AND (DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") >= "'.$curr_date.'" AND  DATE_FORMAT(`FB`.`datetime`,"%Y-%m-%d") <= "'.$range_date.'") ';
      }
      // ( DATE(`FB`.`datetime`) BETWEEN "'.$curr_date.'" AND "'.$range_date.'") AND ';
  		if(!empty($department) && $department != 'All'){
  			  $query .=' AND `SE`.`department` = "'.strtolower($department).'" ';
      }
  		$retrive = $this->db->query($query);
  		$pdfData = $retrive->result_array();
      $returnArray['pdf']  = $pdfData;
      return $returnArray;
    }
}
