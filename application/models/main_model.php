<?php

class Main_model extends CI_Model
{
    public function existance($table, $where) {
          $result = $this->db->select('*')->where($where)->get($table)->row();
        
          print_r($result);
          if(sizeof($result) >= 1){
              return $result;
          }else{

          return FALSE;
          }
      }
      
      public function semi_existance($table, $where) {
          $this->db->where($where);
          $this->db->where('contract_end <=','1971-01-01');
          $this->db->where('create_date >=','2020-09-20');
          $result = $this->db->get($table);

          if($result->num_rows() >= 1){
              return TRUE;
          }

          return FALSE;
      }
      
       public function semi_existance_process($table, $where) {
          $this->db->where($where);
          $this->db->where('create_date >','2020-09-20');
          $result = $this->db->get($table);

          if($result->num_rows() >= 1){
              return TRUE;
          }

          return FALSE;
      }
	
	public function read_table($table, $where=array(),$orderBy='id'){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->order_by($orderBy,'asc');
        $query = $this->db->get();
        return $query->result();

    }

    public function addAndGetID($table, $data){
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function add($table, $data) {
        $this->db->insert($table, $data);
        return true;
    }


    public function contract(){
    	$sql = "SELECT customer_contract.id, `contract_no`,`postcode`, `customer_contract`.`address1`, `customer_contract`.`travel_time`, `market_segment`, `customer_contract`.`service_frequency`, `contract_start`,`contract_end`,customer.account_no,contract_type.name as contract_type,support_type.name as support_type
		FROM `customer_contract`
		LEFT JOIN customer on customer_contract.customer_id=customer.id
		LEFT JOIN contract_type on customer_contract.contract_type=contract_type.id
		LEFT JOIN support_type on customer_contract.support_type=support_type.id";
    	$query = $this->db->query($sql);
        return $query->result();
    }

    public function system(){
    	$sql = "SELECT customer_contract.contract_no, system.system_name,(select `name` from system_type where system.system_type=system_type.id) as type, `contract_system`.`service_frequency`,`implementation_date`,`last_audit_date` FROM `contract_system` 
		LEFT JOIN `system` on system.id=contract_system.system_id
		LEFT JOIN `customer_contract` on customer_contract.id=contract_system.`contract_id`";
    	$query = $this->db->query($sql);
        return $query->result();
    }


    public function sla(){
    	$sql = "SELECT customer_contract.contract_no, sla.name,(select `name` from sla_type where sla.sla_type=sla_type.id) as type,sla.description, `contract_sla`.`response_time`,`resolution_time`,`target` FROM `contract_sla` 
		LEFT JOIN `sla` on sla.id=contract_sla.sla_id
		LEFT JOIN `customer_contract` on customer_contract.id=contract_sla.`contract_id`";
    	$query = $this->db->query($sql);
        return $query->result();
    }

    public function contract_contact($id){
    	$sql = "SELECT `contact_name`,`contact_no`,`contact_email`, (SELECT `name` from `contact_type` WHERE contact_type.id=contract_contact.contact_type)as type FROM `contract_contact` WHERE contract_id=".$id;
    	$query = $this->db->query($sql);
        return $query->result();
    }

     // update table data
    public function update($table, $data, $where= array()){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
        return true;
    }
    
        public function semi_update($table, $data, $contract_no, $address){
        $this->db->set($data);
        $this->db->where('contract_no',$contract_no);
        $this->db->where('address1',$address);
        $this->db->where('contract_end <=','1971-01-01');
        $this->db->where('create_date >=','2020-09-20');
        $this->db->update($table);
        return true;
    }
    
     public function date_semi_update($table, $data, $contract_no, $address){
        $this->db->set($data);
        $this->db->where('contract_no',$contract_no);
        $this->db->where('address1',$address);
        $this->db->where('create_date >','2020-09-20');
        $this->db->update($table);
        return true;
    }

}

?>







