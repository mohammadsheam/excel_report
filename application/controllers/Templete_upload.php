<?php 

/**
 * 	 upload From excel templete
 */
class Templete_upload extends CI_Controller{
	
	function __construct() {	
		parent::__construct();
		$this->load->library('Spreedsheet');
		$this->load->model('main_model');
	}

	public function index(){
		//$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		//$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		$path = 'asset/CopyofCompanyContractTemplate.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
		$Reader-> ChangeSheet(0);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				//var_dump($row);die();
				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
					$company_name = $row[0];
					$account_no = $row[1];
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);
					//echo "<pre>";print_r($data);echo "</pre>";
					
					$where = array('account_no' => $account_no);
					if($this->main_model->existance('customer', $where) == FALSE){
						$this->main_model->add('customer', $data);
					}
				}
			}
		}


		//die();

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = $row[0];
					$contract_no            = $row[1];
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code				= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= $row[16];
					$contact_supervisor_no	= $row[17];
					$contact_supervisor_email= $row[18];
					$contact_service_eng    = $row[19];
					$contact_service_no     = $row[20];
					$contact_service_email  = $row[21];
					$contact_acc_manager    = $row[22];
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';

					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
						'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
					);

					if($this->main_model->existance('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}else{
							$customer_id = 0;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_type
							);
							$contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);

						}



						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}else{
							$support_type_data = array(
								'name'=>$support_type
							);
							$support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);

						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_status
							);
							$contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);

						}


						// add market segment to database table
						$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
						if(($market_segment_check) == FALSE){
							$data = array(
								'name'=>$market_segment
							);
							$this->main_model->add('contract_market_segment',$data);
						}


						$customer_contract_data = array(
							'contract_no'       =>$contract_no,
							'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);


						$contract_id = $this->main_model->addAndGetID('customer_contract',$customer_contract_data);


						// add data to `contract_contact` table
						$supervisor_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>1,
							'contact_name'  =>$contact_supervisor,
							'contact_no'    =>$contact_supervisor_no,
							'contact_email' =>$contact_supervisor_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$supervisor_data);


						$acc_manager_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>2,
							'contact_name'  =>$contact_acc_manager,
							'contact_no'    =>$contact_acc_manager_no,
							'contact_email' =>$contact_acc_manager_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$acc_manager_data);


						$service_eng_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>3,
							'contact_name'  =>$contact_service_eng,
							'contact_no'    =>$contact_service_no,
							'contact_email' =>$contact_service_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$service_eng_data);



						$customer_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>5,
							'contact_name'  =>$contact_customer,
							'contact_no'    =>$contact_customer_no,
							'contact_email' =>$contact_customer_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$customer_data);

					}else{

						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}

						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}


						// update
						$customer_contract_data = array(
							//'contract_no'       =>$contract_no,
							//'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
						);

						$this->main_model->update('customer_contract', $customer_contract_data, $where);

					}
				}
				
			}
		}
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////


		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(2);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
					$contract_no = $row[0];
					$system_name = $row[1];
					$system_type = $row[2];
					$system_desc = $row[3];
					$service_frequency = $row[4];
					//$impl_date = $row[5];
					//$last_audit = $row[6];

					// get system Id
					$system_details = $this->main_model->read_table('system', array('system_name' => $system_name));
					
					if(sizeof($system_details) > 0){
						$system_id = $system_details[0]->id;
					}else{

						// get system type
						$system_type_info = $this->main_model->read_table('system_type', array('name' => $system_type));
						if(sizeof($system_type_info) > 0){
							$system_type_id = $system_type_info[0]->id;
						}else{
							$system_type_data = array('name' => $system_type);
							$system_type_id = $this->main_model->addAndGetID('system_type',$system_type_data);
						}

						$system_data = array(
							'system_name'=> $system_name,
							'system_type'=> $system_type_id,
							'system_desc'=> $system_desc,
							'create_date'=> date('Y-m-d H:i:s')
						);

						$system_id = $this->main_model->addAndGetID('system',$system_data);
					}

					// get contract id
					$contract_info = $this->main_model->read_table('customer_contract', array('contract_no' => $contract_no));

					if(sizeof($contract_info) > 0){
						$contract_id = $contract_info[0]->id;
						$customer_id = $contract_info[0]->customer_id;
					}else{
						$contract_id = 0;
						$customer_id = 0;
					}

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       =>$contract_id,
						'system_id'         =>$system_id,
						'customer_id'       =>$customer_id,
						'service_frequency' =>$service_frequency,
						//'implementation_date' =>$impl_date,
						//'last_audit_date' =>$last_audit,
					);
					
					$this->main_model->add('contract_system',$contract_system_data);
				}
			}
		}

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////


		//////////////////////////////
		//read sla sheet start here //
		//////////////////////////////
		$Reader-> ChangeSheet(3);
		
		////////////////////////////
		//read sla sheet end here //
		////////////////////////////

		echo "Process done:".$path; 

	}

	public function date_data_process(){
		//$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		//$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		$path = 'asset/CopyofCompanyContractTemplate.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = $row[0];
					$contract_no            = $row[1];
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code		= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= $row[16];
					$contact_supervisor_no	= $row[17];
					$contact_supervisor_email= $row[18];
					$contact_service_eng    = $row[19];
					$contact_service_no     = $row[20];
					$contact_service_email  = $row[21];
					$contact_acc_manager    = $row[22];
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';

					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						//'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
//						'contract_end' => '1970-01-01 00:00:00',
					);

					if($this->main_model->semi_existance_process('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						echo $account_no. ' No-';
						
						//$this->main_model->add('contract_contact',$customer_data);

					}else{
						echo $account_no. ' Yes-';
						// get customer id					
//						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
//						if(sizeof($customer) > 0){
//							$customer_id = $customer[0]->id;
//						}
//
//
//						//retrive contract_tupe id
//						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
//						if(sizeof($contract_type_details) > 0){
//							$contract_type_id = $contract_type_details[0]->id;
//						}
//
//						//retrive support_type
//						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
//						if(sizeof($support_type_details) > 0){
//							$support_type_id = $support_type_details[0]->id;
//						}
//
//
//						// contract_status
//						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
//						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
//						if(sizeof($contract_status_details) > 0){
//							$contract_status_id = $contract_status_details[0]->id;
//						}


						// update
						$customer_contract_data = array(
							
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							
						);

						$this->main_model->date_semi_update('customer_contract', $customer_contract_data, $contract_no,$address1);

					}
				}
				
			}
		}
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////


		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		

		echo "Process done:".$path; 

	}

	public function data_process1(){
		$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		//$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		//$path = 'asset/CopyofCompanyContractTemplate.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
//		$Reader-> ChangeSheet(0);
//		foreach ($Reader as $key=> $row){
//			if($key > 0){
//				//var_dump($row);die();
//				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
//					$company_name = $row[0];
//					$account_no = $row[1];
//					$data = array(
//						'account_no' => $account_no,
//						'company_name' => $company_name,
//					);
//					//echo "<pre>";print_r($data);echo "</pre>";
//					
//					$where = array('account_no' => $account_no);
//					if($this->main_model->existance('customer', $where) == FALSE){
//						$this->main_model->add('customer', $data);
//					}
//				}
//			}
//		}


		//die();

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = $row[0];
					$contract_no            = $row[1];
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code		= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= $row[16];
					$contact_supervisor_no	= $row[17];
					$contact_supervisor_email= $row[18];
					$contact_service_eng    = $row[19];
					$contact_service_no     = $row[20];
					$contact_service_email  = $row[21];
					$contact_acc_manager    = $row[22];
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';

					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						//'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
//						'contract_end' => '1970-01-01 00:00:00',
					);

					if($this->main_model->semi_existance('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						
						echo $account_no.'- No -'; 
						//$this->main_model->add('contract_contact',$customer_data);

					}else{
						echo $account_no.'-ok-'; 
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}

						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}


						// update
						$customer_contract_data = array(
							
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
						);

						$this->main_model->semi_update('customer_contract', $customer_contract_data, $contract_no,$address1);

					}
				}
				
			}
		}
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////


		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		

		echo "Process done:".$path; 

	}

	public function data_process2(){
		//$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		//$path = 'asset/CopyofCompanyContractTemplate.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
//		$Reader-> ChangeSheet(0);
//		foreach ($Reader as $key=> $row){
//			if($key > 0){
//				//var_dump($row);die();
//				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
//					$company_name = $row[0];
//					$account_no = $row[1];
//					$data = array(
//						'account_no' => $account_no,
//						'company_name' => $company_name,
//					);
//					//echo "<pre>";print_r($data);echo "</pre>";
//					
//					$where = array('account_no' => $account_no);
//					if($this->main_model->existance('customer', $where) == FALSE){
//						$this->main_model->add('customer', $data);
//					}
//				}
//			}
//		}


		//die();

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = $row[0];
					$contract_no            = $row[1];
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code		= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= $row[16];
					$contact_supervisor_no	= $row[17];
					$contact_supervisor_email= $row[18];
					$contact_service_eng    = $row[19];
					$contact_service_no     = $row[20];
					$contact_service_email  = $row[21];
					$contact_acc_manager    = $row[22];
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';

					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						//'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
//						'contract_end' => '1970-01-01 00:00:00',
					);

					if($this->main_model->semi_existance('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						
						echo $account_no. 'No -';
						//$this->main_model->add('contract_contact',$customer_data);

					}else{
						echo $account_no. " process - ";
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}

						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}


						// update
						$customer_contract_data = array(
							
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							
						);

						$this->main_model->semi_update('customer_contract', $customer_contract_data, $contract_no,$address1);

					}
				}
				
			}
		}
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////


		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		

		echo "Process done:".$path; 

	}




	public function customerOnly(){
		echo "start";
		$path = 'asset/CUSTOMERMASTERDATABASE(FROMSAP).xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
		$Reader-> ChangeSheet(0);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				//var_dump($row);die();
				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
					$company_name = $row[0];
					$account_no = $row[1];
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);
					//echo "<pre>";print_r($data);echo "</pre>";
					
					$where = array('account_no' => $account_no);
					if($this->main_model->existance('customer', $where) == FALSE){
						$this->main_model->add('customer', $data);
					}
				}
			}
		}	// foreach end here
		echo "End";
	}



	/**
	 * Crossed/strike out data  will remove
     * Blank contract no  NA
	 * End date  blank/”1970-01-01”
	 *
	 */
	public function manipulate(){
		//$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		//$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		//$path = 'asset/CopyofCompanyContractTemplate.xlsx';
		$path = 'asset/5th_template.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
		$Reader-> ChangeSheet(0);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				//var_dump($row);die();
				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
					$company_name = $row[0];
					$account_no = $row[1];
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);
					//echo "<pre>";print_r($data);echo "</pre>";
					
					$where = array('account_no' => $account_no);
					if($this->main_model->existance('customer', $where) == FALSE){
						//$this->main_model->add('customer', $data);
					}
				}
			}
		}


		//die();

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = ($row[1] !='')?$row[1]:'A/N';;
					$contract_no            = ($row[1] !='')?$row[1]:'N/A';
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code				= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= isset($row[16])?$row[16]:'';
					$contact_supervisor_no	= isset($row[17])?$row[17]:'';
					$contact_supervisor_email= isset($row[18])?$row[18]:'';
					$contact_service_eng    = isset($row[19])?$row[19]:'';
					$contact_service_no     = isset($row[20])?$row[20]:'';
					$contact_service_email  = isset($row[21])?$row[21]:'';
					$contact_acc_manager    = isset($row[22])?$row[22]:'';
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';


					echo "<pre>";
					echo "id: "; print_r($key+1);
					echo "\naccount_no: "; print_r($account_no);
					echo "\naddress1: "; print_r($address1);
					echo "\nContract No: "; print_r($contract_no);
					//echo "<pre>";
					// echo "<pre>";
					// print_r($contract_type);
					// echo "<pre>";
					// print_r($contract_status);
					// echo "<pre>";
					// print_r($post_code);
					// echo "<pre>";
					// print_r($address1);
					// echo "<pre>";
					// print_r($address2);
					// echo "<pre>";
					// print_r($travel_time);
					// echo "<pre>";
					// print_r($market_segment);
					// echo "<pre>";
					// print_r($service_frequency);
					// echo "<pre>";
					// print_r($support_type);
					// echo "<pre>";
					// print_r($contract_start);
					// echo "<pre>";
					// print_r($contract_end);
					// echo "<pre>";
					// print_r($contact_customer);
					// echo "<pre>";
					// print_r($contact_customer_no);
					// echo "<pre>";
					// print_r($contact_customer_email);
					// echo "<pre>";
					// print_r($contact_supervisor);
					// echo "<pre>";
					// print_r($contact_supervisor_no);
					// echo "<pre>";
					// print_r($contact_supervisor_email);
					// echo "<pre>";
					// print_r($contact_service_eng);
					// echo "<pre>";
					// print_r($contact_service_no);
					// echo "<pre>";
					// print_r($contact_service_email);
					// echo "<pre>";
					// print_r($contact_acc_manager);
					// echo "<pre>";
					// print_r($contact_acc_manager_no);
					// echo "<pre>";
					// print_r($contact_acc_manager_email);
					// echo "<pre>";
					// print_r($department);
					// echo "<pre>";

					
					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
						'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
					);

					/*if($this->main_model->existance('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}else{
							$customer_id = 0;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_type
							);
							$contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);

						}


						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}else{
							$support_type_data = array(
								'name'=>$support_type
							);
							$support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);

						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_status
							);
							$contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);

						}


						// add market segment to database table
						$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
						if(($market_segment_check) == FALSE){
							$data = array(
								'name'=>$market_segment
							);
							$this->main_model->add('contract_market_segment',$data);
						}


						$customer_contract_data = array(
							'contract_no'       =>$contract_no,
							'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);

						$contract_id = $this->main_model->addAndGetID('customer_contract',$customer_contract_data);

						// add data to `contract_contact` table
						$supervisor_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>1,
							'contact_name'  =>$contact_supervisor,
							'contact_no'    =>$contact_supervisor_no,
							'contact_email' =>$contact_supervisor_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$supervisor_data);

						$acc_manager_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>2,
							'contact_name'  =>$contact_acc_manager,
							'contact_no'    =>$contact_acc_manager_no,
							'contact_email' =>$contact_acc_manager_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$acc_manager_data);

						$service_eng_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>3,
							'contact_name'  =>$contact_service_eng,
							'contact_no'    =>$contact_service_no,
							'contact_email' =>$contact_service_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$service_eng_data);

						$customer_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>5,
							'contact_name'  =>$contact_customer,
							'contact_no'    =>$contact_customer_no,
							'contact_email' =>$contact_customer_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$customer_data);
						
					}else{
						
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}

						//retrive contract_type id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_type
							);
							$contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);

						}

						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}else{
							$support_type_data = array(
								'name'=>$support_type
							);
							$support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);

						}

						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}

						// update
						$customer_contract_data = array(
							//'contract_no'       =>$contract_no,
							//'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
						);

						$this->main_model->update('customer_contract', $customer_contract_data, $where);
						
					}*/ // existance condition end here
				} // isset condition end here
				
			}
		} // foreach end here
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////

		die();
		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(2);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
					$contract_no = $row[0];
					$system_name = $row[1];
					$system_type = $row[2];
					$system_desc = $row[3];
					$service_frequency = $row[4];
					//$impl_date = $row[5];
					//$last_audit = $row[6];

					// get system Id
					$system_details = $this->main_model->read_table('system', array('system_name' => $system_name));
					
					if(sizeof($system_details) > 0){
						$system_id = $system_details[0]->id;
					}else{

						// get system type
						$system_type_info = $this->main_model->read_table('system_type', array('name' => $system_type));
						if(sizeof($system_type_info) > 0){
							$system_type_id = $system_type_info[0]->id;
						}else{
							$system_type_data = array('name' => $system_type);
							$system_type_id = $this->main_model->addAndGetID('system_type',$system_type_data);
						}

						$system_data = array(
							'system_name'=> $system_name,
							'system_type'=> $system_type_id,
							'system_desc'=> $system_desc,
							'create_date'=> date('Y-m-d H:i:s')
						);

						$system_id = $this->main_model->addAndGetID('system',$system_data);
					}

					// get contract id
					$contract_info = $this->main_model->read_table('customer_contract', array('contract_no' => $contract_no));

					if(sizeof($contract_info) > 0){
						$contract_id = $contract_info[0]->id;
						$customer_id = $contract_info[0]->customer_id;
					}else{
						$contract_id = 0;
						$customer_id = 0;
					}

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       =>$contract_id,
						'system_id'         =>$system_id,
						'customer_id'       =>$customer_id,
						'service_frequency' =>$service_frequency,
						//'implementation_date' =>$impl_date,
						//'last_audit_date' =>$last_audit,
					);
					
					$this->main_model->add('contract_system',$contract_system_data);
				}
			}
		}

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////


		//////////////////////////////
		//read sla sheet start here //
		//////////////////////////////
		$Reader-> ChangeSheet(3);
		
		////////////////////////////
		//read sla sheet end here //
		////////////////////////////

		echo "Process done:".$path; 

	}






	public function onlySystemUpload(){
		//ini_set('MAX_EXECUTION_TIME', '-1');
		$path = 'asset/CompanyContractTemplate-ESSERVICE-changesforNetlink&UOBContract.xlsx';
		$Reader = new SpreadsheetReader($path);

		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(2);
                
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
                                        //print_r($row);
					$contract_no = $row[0];
					$system_name = $row[1];
					$system_type = $row[2];
					$system_desc = $row[3];
					$service_frequency = $row[4];
					//$impl_date = $row[5];
					//$last_audit = $row[6];
                                        if($contract_no != ''){
                                         print_r($row);   
					// get system Id
					$system_details = $this->main_model->read_table('system', array('system_name' => $system_name));
					
					if(sizeof($system_details) > 0){
						$system_id = $system_details[0]->id;
					}else{

						// get system type
						$system_type_info = $this->main_model->read_table('system_type', array('name' => $system_type));
						if(sizeof($system_type_info) > 0){
							$system_type_id = $system_type_info[0]->id;
						}else{
							$system_type_data = array('name' => $system_type);
							$system_type_id = $this->main_model->addAndGetID('system_type',$system_type_data);
						}

						$system_data = array(
							'system_name'=> $system_name,
							'system_type'=> $system_type_id,
							'system_desc'=> $system_desc,
							'create_date'=> date('Y-m-d H:i:s')
						);

						$system_id = $this->main_model->addAndGetID('system',$system_data);
					}

					// get contract id
					$contract_info = $this->main_model->read_table('customer_contract', array('contract_no' => $contract_no));
					 echo sizeof($contract_info);
					 echo "\n";
					 echo "<pre>";
					 print_r($contract_info);
					 //die();


					if(sizeof($contract_info) > 0){

						foreach ($contract_info as $key => $value) {
							
							//now relate contract with system
							$contract_system_data = array(
								'contract_id'       =>$value->id,
								'system_id'         =>$system_id,
								'customer_id'       =>$value->customer_id,
								'service_frequency' =>$service_frequency,
								//'implementation_date' =>$impl_date,
								//'last_audit_date' =>$last_audit,
							);
							//echo "<pre>";
							//print_r($contract_system_data);
							$this->main_model->add('contract_system',$contract_system_data);

						}
					}

				}
			}
		}
                
                }

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////
	}

public function blank_cust_contract_update_regain(){
		//ini_set('MAX_EXECUTION_TIME', '-1');
		$path = 'asset/regain.xlsx';
		$Reader = new SpreadsheetReader($path);
return TRUE;
		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(0);
                
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
                                        //print_r($row);
					$customer_id = $row[0];
					$contract_id = $row[1];
					$account_no = $row[2];
					$company_name = $row[3];
					$contract_no = $row[4];
                                        $address = $row[5];
                                        $contract_end = $row[6];
                                        $contract_end = date('Y-m-d H:i:s',strtotime($contract_end));
					//$impl_date = $row[5];
					//$last_audit = $row[6];
                                        echo "<pre>"; 
                                        if($account_no != ''){
//                                         print_r($row);   
					//first update customer infor with customer_id
                                         $cust_data = array(
                                             'account_no' =>$account_no,
                                             'company_name'=>$company_name
                                         );
                                         
                                         print_r($cust_data);
                                         
                                         $where = array(
							'id' => $customer_id
						);

						$this->main_model->update('customer', $cust_data, $where);
                                         
                                         //then update contract info with contract id
                                         $cust_contract_data = array(
                                             'contract_no' =>$account_no,
                                             'address1'=>$address,
                                             'contract_end'=>$contract_end
                                         );
                                         
                                         print_r($cust_contract_data);
                                         
                                         $where = array(
							'id' => $contract_id
						);

					$this->main_model->update('customer_contract', $cust_contract_data, $where);
					
					

				}
			}
		}
                
                }

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////
	}
        
        
        public function blank_cust_contract_update_rain(){
		//ini_set('MAX_EXECUTION_TIME', '-1');
		$path = 'asset/rain.xlsx';
		$Reader = new SpreadsheetReader($path);
return TRUE;
		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(0);
                
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
                                        //print_r($row);
					$customer_id = $row[0];
					$contract_id = $row[1];
					$account_no = $row[2];
					$company_name = $row[3];
					$contract_no = $row[4];
                                        $address = $row[5];
                                        $contract_end = $row[6];
                                        $contract_end = date('Y-m-d H:i:s',strtotime($contract_end));
					//$impl_date = $row[5];
					//$last_audit = $row[6];
                                        echo "<pre>"; 
                                        if($account_no != ''){
//                                         print_r($row);   
					//first update customer infor with customer_id
                                         $cust_data = array(
                                             'account_no' =>$account_no,
                                             'company_name'=>$company_name
                                         );
                                         
                                         print_r($cust_data);
                                         
                                         $where = array(
							'id' => $customer_id
						);

						$this->main_model->update('customer', $cust_data, $where);
                                         
                                         //then update contract info with contract id
                                         $cust_contract_data = array(
                                             'contract_no' =>$account_no,
                                             'address1'=>$address,
                                             'contract_end'=>$contract_end
                                         );
                                         
                                         print_r($cust_contract_data);
                                         
                                         $where = array(
							'id' => $contract_id
						);

					$this->main_model->update('customer_contract', $cust_contract_data, $where);
					
					

				}
			}
		}
                
                }

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////
	}
        
        
        public function dataprocess_nov(){
		//$path = 'asset/CompanyContractTemplate-ESSERVICE.xlsx';
		//$path = 'asset/CompanyContractTemplate-ThirdParty,CTC&Safe.xlsx';
		$path = 'asset/dataprocess_novFORSINGAPOREPOWERGRID.xlsx';
		$Reader = new SpreadsheetReader($path);

		// read company worksheet
		$Reader-> ChangeSheet(0);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				//var_dump($row);die();
				if(( isset($row[0]) && $row[0]!='' ) && ( isset($row[1]) && $row[1] !='') ){
					$company_name = $row[0];
					$account_no = $row[1];
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);
					//echo "<pre>";print_r($data);echo "</pre>";
					
					$where = array('account_no' => $account_no);
					if($this->main_model->existance('customer', $where) == FALSE){
						$this->main_model->add('customer', $data);
					}
				}
			}
		}


		//die();

		// read contract worksheet
		$Reader-> ChangeSheet(1);
		foreach ($Reader as $key=> $row){
			if($key > 0){

				if(isset($row)){
					
					// get value
					$account_no             = $row[0];
					$contract_no            = $row[1];
					$contract_type          = $row[2];
					$contract_status        = $row[3];
					$post_code				= $row[4];
					$address1               = $row[5];
					$address2               = $row[6];
					$travel_time            = $row[7];
					$market_segment         = $row[8];
					$service_frequency      = $row[9];
					$support_type           = $row[10];
					$contract_start         = $row[11];
					$contract_end           = $row[12];
					$contact_customer       = $row[13];
					$contact_customer_no    = $row[14];
					$contact_customer_email = $row[15];
					$contact_supervisor		= $row[16];
					$contact_supervisor_no	= $row[17];
					$contact_supervisor_email= $row[18];
					$contact_service_eng    = $row[19];
					$contact_service_no     = $row[20];
					$contact_service_email  = $row[21];
					$contact_acc_manager    = isset($row[22])?$row[22]:'';
					$contact_acc_manager_no = isset($row[23])?$row[23]:'';
					$contact_acc_manager_email= isset($row[24])?$row[24]:'';
					$department             = isset($row[25])?$row[25]:'';

					// check already exist or not
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						//'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
						//'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
					);

					if($this->main_model->existance('customer_contract', $where) == FALSE){ // false means not exit
						// add new entry
						echo "new"; 
						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}else{
							$customer_id = 0;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_type
							);
							$contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);

						}



						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}else{
							$support_type_data = array(
								'name'=>$support_type
							);
							$support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);

						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}else{
							$contract_type_data = array(
								'name'=>$contract_status
							);
							$contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);

						}


						// add market segment to database table
						$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
						if(($market_segment_check) == FALSE){
							$data = array(
								'name'=>$market_segment
							);
							$this->main_model->add('contract_market_segment',$data);
						}


						$customer_contract_data = array(
							'contract_no'       =>$contract_no,
							'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);


						$contract_id = $this->main_model->addAndGetID('customer_contract',$customer_contract_data);


						// add data to `contract_contact` table
						$supervisor_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>1,
							'contact_name'  =>$contact_supervisor,
							'contact_no'    =>$contact_supervisor_no,
							'contact_email' =>$contact_supervisor_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$supervisor_data);


						$acc_manager_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>2,
							'contact_name'  =>$contact_acc_manager,
							'contact_no'    =>$contact_acc_manager_no,
							'contact_email' =>$contact_acc_manager_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$acc_manager_data);


						$service_eng_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>3,
							'contact_name'  =>$contact_service_eng,
							'contact_no'    =>$contact_service_no,
							'contact_email' =>$contact_service_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$service_eng_data);



						$customer_data = array(
							'contract_id'   =>$contract_id,
							'contact_type'  =>5,
							'contact_name'  =>$contact_customer,
							'contact_no'    =>$contact_customer_no,
							'contact_email' =>$contact_customer_email,
							'creator_id'    =>1,
							'create_date'   =>date('Y-m-d H:i:s')
						);
						
						$this->main_model->add('contract_contact',$customer_data);

					}else{
                                            echo "old"; 

						// get customer id					
						$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
						if(sizeof($customer) > 0){
							$customer_id = $customer[0]->id;
						}


						//retrive contract_tupe id
						$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
						if(sizeof($contract_type_details) > 0){
							$contract_type_id = $contract_type_details[0]->id;
						}

						//retrive support_type
						$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
						if(sizeof($support_type_details) > 0){
							$support_type_id = $support_type_details[0]->id;
						}


						// contract_status
						$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
						//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
						if(sizeof($contract_status_details) > 0){
							$contract_status_id = $contract_status_details[0]->id;
						}


						// update
						$customer_contract_data = array(
							//'contract_no'       =>$contract_no,
							//'contract_name'     =>$contract_no,
							'customer_id'       =>$customer_id,
							'contract_type'     =>$contract_type_id,
							'support_type'      =>$support_type_id,
							'address1'          =>$address1,
							'address2'          =>$address2,
							'travel_time'       =>$travel_time,
							'postcode'          => $post_code,
							'department'        => $department,
							'contract_start'    =>date('Y-m-d H:i:s',strtotime($contract_start)),
							'contract_end'      =>date('Y-m-d H:i:s',strtotime($contract_end)),
							'market_segment'    =>$market_segment,
							'service_frequency' =>$service_frequency, 
							'status'            =>$contract_status_id,
							'create_by'         =>1,
							'create_date'       =>date('Y-m-d H:i:s'),

						);

						$where = array(
							'contract_no' => $contract_no,
							'address1' => $address1,
							//'contract_start' =>date('Y-m-d H:i:s',strtotime($contract_start)),
							//'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
						);

						$this->main_model->update('customer_contract', $customer_contract_data, $where);

					}
				}
				
			}
		}
		
		//////////////////////////////////
		// read contract sheet end here //
		//////////////////////////////////


		//////////////////////////////////
		// read system sheet start here //
		//////////////////////////////////
		$Reader-> ChangeSheet(2);
		foreach ($Reader as $key=> $row){
			if($key > 0){
				if(isset($row)){
					$contract_no = $row[0];
					$system_name = $row[1];
					$system_type = $row[2];
					$system_desc = $row[3];
					$service_frequency = $row[4];
					//$impl_date = $row[5];
					//$last_audit = $row[6];

					// get system Id
					$system_details = $this->main_model->read_table('system', array('system_name' => $system_name));
					
					if(sizeof($system_details) > 0){
						$system_id = $system_details[0]->id;
					}else{

						// get system type
						$system_type_info = $this->main_model->read_table('system_type', array('name' => $system_type));
						if(sizeof($system_type_info) > 0){
							$system_type_id = $system_type_info[0]->id;
						}else{
							$system_type_data = array('name' => $system_type);
							$system_type_id = $this->main_model->addAndGetID('system_type',$system_type_data);
						}

						$system_data = array(
							'system_name'=> $system_name,
							'system_type'=> $system_type_id,
							'system_desc'=> $system_desc,
							'create_date'=> date('Y-m-d H:i:s')
						);

						$system_id = $this->main_model->addAndGetID('system',$system_data);
					}

					// get contract id
					$contract_info = $this->main_model->read_table('customer_contract', array('contract_no' => $contract_no));

					if(sizeof($contract_info) > 0){
                                            foreach($contract_info as $info){
                                                $contract_id = $info->id;
						$customer_id = $info->customer_id;
                                                
                                                $contract_system_data = array(
						'contract_id'       =>$contract_id,
						'system_id'         =>$system_id,
						'customer_id'       =>$customer_id,
						'service_frequency' =>$service_frequency,
						//'implementation_date' =>$impl_date,
						//'last_audit_date' =>$last_audit,
					);
					
					//$this->main_model->add('contract_system',$contract_system_data);
                                                
                                            }
						
					}else{
						$contract_id = 0;
						$customer_id = 0;
					}

					//now relate contract with system
					
				}
			}
		}

		////////////////////////////////
		// read system sheet end here //
		////////////////////////////////


		//////////////////////////////
		//read sla sheet start here //
		//////////////////////////////
		$Reader-> ChangeSheet(3);
		
		////////////////////////////
		//read sla sheet end here //
		////////////////////////////

		echo "Process done:".$path; 

	}


}



?>

<!--$slot = array(
    [0]=>array(
        'start_time'=>'2020-09-23 08:30',
        'end_time'=>'2020-09-23 15:20',
        'type'=>'rg'
    ),
    [1]=>array(
        'start_time'=>'2020-09-23 14:30',
        'end_time'=>'2020-09-23 17:30',
        'type'=>'rg'
    ),
    [2]=>array(
        'start_time'=>'2020-09-23 17:30',
        'end_time'=>'2020-09-23 21:20',
        'type'=>'ot'
    )
);-->