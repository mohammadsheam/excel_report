<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
class Excel_import extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('excel_import_model');
		$this->load->model('main_model');
		//$this->load->library('Spreed');
		$this->load->helper('test_helper');
	}

	function index()
	{	
		$this->load->view('excel_import');
	}
	


public function test(){
	$this->load->helper('download');
	$path = FCPATH.'\asset\Company Contract Template.xlsx';
	$reader = IOFactory::createReader('Xlsx');
	$reader->setLoadAllSheets();
	$spreadsheet = $reader->load($path);

	$loadedSheetNames = $spreadsheet->getSheetNames();
	//print_r($loadedSheetNames);die();
	//$spreadsheet = new Spreadsheet();
	$writer  = new Xlsx($spreadsheet);
	

	// read company sheet data
	$spreadsheet->setActiveSheetIndex(0);
	$sheet = $spreadsheet->getActiveSheet();
	$companyData = $this->main_model->read_table('customer');
	$rowCount = 2;
	foreach ($companyData as $key => $value) {

	    $sheet->setCellValue('A' . $rowCount, $value->company_name);
	    $sheet->setCellValue('B' . $rowCount, $value->account_no);
	    $rowCount++;
	}



	// read contract sheet data
	$spreadsheet->setActiveSheetIndex(1);
	$sheet = $spreadsheet->getActiveSheet();
	$contractData = $this->main_model->contract();
	$rowCount = 2;
	foreach ($contractData as $key => $value) {

	    $sheet->setCellValue('A' . $rowCount, $value->account_no);
	    $sheet->setCellValue('B' . $rowCount, $value->contract_no);
	    $sheet->setCellValue('C' . $rowCount, $value->contract_type);
	    $sheet->setCellValue('D' . $rowCount, '');
	    $sheet->setCellValue('E' . $rowCount, $value->postcode);
	    $sheet->setCellValue('F' . $rowCount, $value->address1);
	    $sheet->setCellValue('G' . $rowCount, $value->travel_time);
	    $sheet->setCellValue('H' . $rowCount, $value->market_segment);
	    $sheet->setCellValue('I' . $rowCount, $value->service_frequency);
	    $sheet->setCellValue('J' . $rowCount, $value->support_type);
	    $sheet->setCellValue('K' . $rowCount, $value->contract_start);
	    $sheet->setCellValue('L' . $rowCount, $value->contract_end);


	    // read contract_contact
	    $contra_info = $this->main_model->contract_contact($value->id);
	    if($contra_info){
	    	foreach ($contra_info  as $info) {
	    		if($info->type=='Customer'){
				    $sheet->setCellValue('M' . $rowCount, $info->contact_name);
				    $sheet->setCellValue('N' . $rowCount, $info->contact_no);
				    $sheet->setCellValue('O' . $rowCount, $info->contact_email);
	    		}elseif($info->type=='Supervisor'){
	    			$sheet->setCellValue('P' . $rowCount, $info->contact_name);
				    $sheet->setCellValue('Q' . $rowCount, $info->contact_no);
				    $sheet->setCellValue('R' . $rowCount, $info->contact_email);
	    		}elseif($info->type=='Service Engineer'){
	    			$sheet->setCellValue('S' . $rowCount, $info->contact_name);
				    $sheet->setCellValue('T' . $rowCount, $info->contact_no);
				    $sheet->setCellValue('U' . $rowCount, $info->contact_email);
	    		}elseif($info->type=='Account Manager'){
				    $sheet->setCellValue('V' . $rowCount, $info->contact_name);
				    $sheet->setCellValue('W' . $rowCount, $info->contact_no);
				    $sheet->setCellValue('X' . $rowCount, $info->contact_email);
	    		}

	    	}
	    }
	    
	    $rowCount++;
	}


	

	// read system sheet data
	$spreadsheet->setActiveSheetIndex(2);
	$sheet = $spreadsheet->getActiveSheet();
	$systemData = $this->main_model->system();
	$rowCount = 2;
	foreach ($systemData as $key => $value) {

	    $sheet->setCellValue('A' . $rowCount, $value->contract_no);
	    $sheet->setCellValue('B' . $rowCount, $value->system_name);
	    $sheet->setCellValue('C' . $rowCount, $value->type);
	    $sheet->setCellValue('D' . $rowCount, '');
	    $sheet->setCellValue('E' . $rowCount, $value->service_frequency);
	    $sheet->setCellValue('F' . $rowCount, $value->implementation_date);
	    $sheet->setCellValue('G' . $rowCount, $value->last_audit_date);
	    $rowCount++;
	}



	
	// read sla sheet data
	$spreadsheet->setActiveSheetIndex(3);
	$sheet = $spreadsheet->getActiveSheet();
	$companyData = $this->main_model->sla();
	$rowCount = 2;
	foreach ($companyData as $key => $value) {

	    $sheet->setCellValue('A' . $rowCount, $value->contract_no);
	    $sheet->setCellValue('C' . $rowCount, $value->type);
	    $sheet->setCellValue('B' . $rowCount, $value->name);
	    $sheet->setCellValue('D' . $rowCount, $value->description);
	    $sheet->setCellValue('E' . $rowCount, $value->response_time);
	    $sheet->setCellValue('F' . $rowCount, $value->resolution_time);
	    $sheet->setCellValue('G' . $rowCount, $value->target);
	    $rowCount++;
	}


	



	
	$fileName = 'Company Contract Template.xlsx';
	$this->output->set_header('Content-Type: application/vnd.ms-excel');
    $this->output->set_header("Content-type: application/csv");
    $this->output->set_header('Cache-Control: max-age=0');
    $writer->save(FCPATH.$fileName); 
    //redirect(HTTP_UPLOAD_PATH.$fileName); 
    $filepath = file_get_contents(FCPATH.$fileName);
    force_download($fileName, $filepath);
}



	function import()
	{


		if(isset($_FILES["file"]["name"]))
		{	

			$path = $_FILES["file"]["tmp_name"];
			$path = APPPATH.'\controllers\test.xlsx';
			$object = PHPExcel_IOFactory::load($path);
			$worksheet = $object->getWorksheetIterator();
			//$worksheet = $object->getAllSheets();
			var_dump($object);die();
			
			
			foreach($object->getWorksheetIterator() as $key => $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				$nameRow = 3;
				//print_r($highestRow);
				//print("\n");
				for($row=1; $row<=$highestRow; $row+=3){

					$teacher_name = $worksheet->getCellByColumnAndRow(0, $nameRow)->getValue();					
					
					for ($cell=1; $cell <= 16; $cell++) { 
						$subFiled = ($worksheet->getCellByColumnAndRow($cell, $row+1)->getValue());
						if($subFiled != ''){
							// get field
							$classFiled = ($worksheet->getCellByColumnAndRow($cell, $row+2)->getValue());
							$classroomFiled = ($worksheet->getCellByColumnAndRow($cell, $row+3)->getValue());

							// get table id
							$sub_id       = getId('subjects',array('subject_short_name' => $subFiled));
							$class_id     = getId('classes',array('class_short_name' => $classFiled));
							$classroom_id = getId('classrooms',array('classroom_short_name' => $classroomFiled));
							$user_id      = getId('users',array('user_short_name' => $teacher_name));


							$data = array(
								'teachers'   => $user_id,
								'semesters'  => 1,
								'subjects'   => $sub_id,
								'days'       => $key+1,
								'week'       => 1,
								'lessons'    => $cell,
								//'classrooms' => $classroom_id,
								'classrooms' => 0,
								'classes'    => $class_id,
							);

							$this->db->insert('routine', $data);

						}
					}

					/*
					//print($worksheet->getCellByColumnAndRow(1, $row+1)->getValue());
					$sub = ($worksheet->getCellByColumnAndRow(1, $row+1)->getValue());
					//("\n");
					//print($worksheet->getCellByColumnAndRow(1, $row+2)->getValue());
					//print("\n");
					//print_r($worksheet->getCellByColumnAndRow(1, $row+2)->getValue());

					$data = array(
						'name' => $teacher_name,
						'sub' => $sub,
						'day' => $key+1,
						'classroom' => $classroom,
						'lesson' => 1,
					);

					print_r($data);
					print("\n");
					print("\n");

					$sub = ($worksheet->getCellByColumnAndRow(2, $row+1)->getValue());
					$classroom= ($worksheet->getCellByColumnAndRow(2, $row+2)->getValue());

					if($sub !=''){

						$data = array(
							'name' => $teacher_name,
							'sub' => $sub,
							'lesson' => 2,
							'day' => $key+1,
							'classroom' => $classroom,
						);
					}
					print_r($data);
					print("\n");
					print("\n");


					$sub = ($worksheet->getCellByColumnAndRow(3, $row+1)->getValue());
					$classroom= ($worksheet->getCellByColumnAndRow(3, $row+2)->getValue());

					if($sub !=''){

						$data = array(
							'name' => $teacher_name,
							'sub' => $sub,
							'lesson' => 3,
							'day' => $key+1,
							'classroom' => $classroom,
						);
					}
					print_r($data);
					print("\n");
					print("\n");
					*/

					$nameRow += 3;
					
				}
				
			}
			
			//$this->excel_import_model->insert($data);
			echo 'Data Imported successfully';
		}	
	}
}

?>