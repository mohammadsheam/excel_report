<?php

/**
 * 
 */
class Common extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('Spreedsheet');
		$this->load->model('main_model');
	}


	public function excel()
	{
		$path = 'asset/excel.xlsx';

		$Reader = new SpreadsheetReader($path);
		$data = [];
		foreach ($Reader as $key => $row) {
			// customer check
			if ($key > 1) {

				$account_no             = $row[0];
				$company_name           = $row[3];

				$where = array('account_no' => $account_no);
				$info = $this->main_model->existance('customer', $where);
				//$info1 = $this->main_model->existance('customer2', $where);

				//echo "<pre>";print_r($info);echo "</pre>";

				//if($info == FALSE && $info1 == FALSE){
				if ($info == FALSE) {
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);

					$this->main_model->add('customer', $data);
				}
			}
		}
	}



	public function contract()
	{
		$path = 'asset/excel.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key > 1) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[0];
				$contract_no            = $row[1];
				$contract_status        = $row[2];
				$company_name           = $row[3];
				$address1               = $row[4];
				$service_frequency      = $row[5];
				$contract_start         = $row[6];
				$contract_end           = $row[7];
				//$auto_renewal         = $row[8];
				$contract_type          = $row[9];
				$system                 = $row[10];
				//$product_type         = $row[11];
				$support_type           = $row[12];
				//$contract_status      = $row[13];
				//$contract_status      = $row[14];
				$contact_acc_manager    = $row[15];
				$contact_customer       = $row[16];
				$contact_customer_no    = $row[17];
				$contact_service_eng    = $row[18];
				$contact_customer_email = $row[19];
				//$contract_status      = $row[20];
				$market_segment         = $row[21];



				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
					'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);
				if ($info == FALSE) {

					//first check and retrive account no id
					//$customer = $this->db->select('*')->where('account_no',$account_no)->get('customer2')->row();
					$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
					if (sizeof($customer) > 0) {
						$customer_id = $customer[0]->id;
					} else {
						//$customer_id = 1; // will not applicable
					}


					//retrive contract_tupe id
					$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
					if (sizeof($contract_type_details) > 0) {
						$contract_type_id = $contract_type_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_type
						);
						$contract_type_id = $this->main_model->addAndGetID('contract_type', $contract_type_data);
					}


					//retrive support_type
					//$support_type = $val['Support type'];

					//$support_type_details = $this->db->select('*')->where('name',$support_type)->get('support_type')->row();
					$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
					if (sizeof($support_type_details) > 0) {
						$support_type_id = $support_type_details[0]->id;
					} else {
						$support_type_data = array(
							'name' => $support_type
						);
						$support_type_id = $this->common_model->addAndGetID('support_type', $support_type_data);
					}

					//retrive post code
					//$address = $val['Customer Account No'];
					$postcode = (int) substr($address1, -7);
					$postcode = (int) $postcode;
					if ($postcode == 0) {
						$postcode = '';
					}


					// contract_status
					$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					if (sizeof($contract_status_details) > 0) {
						$contract_status_id = $contract_status_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_status
						);
						$contract_status_id = $this->main_model->addAndGetID('contract_status', $contract_type_data);
					}

					// add market segment to database table
					$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
					if (($market_segment_check) == FALSE) {
						$data = array(
							'name' => $market_segment
						);
						$this->main_model->add('contract_market_segment', $data);
					}


					$customer_contract_data = array(
						'contract_no'       => $contract_no,
						'contract_name'     => $contract_no,
						'customer_id'       => $customer_id,
						'contract_type'     => $contract_type_id,
						'support_type'      => $support_type_id,
						'address1'          => $address1,
						'address2'          => 'duplicate',
						'postcode'          => $postcode,
						'department'        => "ES",
						'contract_start'    => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end'      => date('Y-m-d H:i:s', strtotime($contract_end)),
						'market_segment'    => $market_segment,
						'service_frequency' => $service_frequency,
						'status'            => $contract_status_id,
						'create_by'         => 1,
						'create_date'       => date('Y-m-d H:i:s'),

					);



					//now check duplicate and insert in database
					$contract_id = $this->main_model->addAndGetID('customer_contract', $customer_contract_data);

					//now save contract system

					//$system_details = $this->db->select('*')->where('system_name',$system)->get('system')->row();
					$system_details = $this->main_model->read_table('system', array('system_name' => $system));

					if (sizeof($system_details) > 0) {
						$system_id = $system_details[0]->id;
					} else {
						$system_data = array(
							'system_name' => $system
						);
						$system_id = $this->main_model->addAndGetID('system', $system_data);
					}

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       => $contract_id,
						'system_id'         => $system_id,
						'customer_id'       => $customer_id,
						'service_frequency' => $service_frequency,
					);

					$contract_system_id = $this->main_model->addAndGetID('contract_system', $contract_system_data);

					//then import contract contact

					//$contact_no = $val['CONTACT NUMBER'];

					if ($contact_acc_manager != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 2,
							'contact_name'    => $contact_acc_manager,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}
					$contact_supervisor = '';
					if ($contact_supervisor != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 1,
							'contact_name'    => $contact_supervisor,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}


					if ($contact_customer != '') {
						$save_data = array(
							'contract_id'   => $contract_id,
							'contact_type'  => 5,
							'contact_name'  => $contact_customer,
							'contact_no'    => $contact_customer_no,
							'contact_email' => $contact_customer_email,
							'creator_id'    => 1,
							'create_date'   => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					if ($contact_service_eng != '') {
						$save_data = array(
							'contract_id' => $contract_id,
							'contact_type' => 3,
							'contact_name' => $contact_service_eng,
							//'contact_no'=>$contact_no,
							//'contact_email'=>$contact_email,
							'creator_id' => 1,
							'create_date' => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					//  customer contract import end

				} else { // update existing record

					$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					if (sizeof($contract_status_details) > 0) {
						$contract_status_id = $contract_status_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_status
						);
						$contract_status_id = $this->main_model->addAndGetID('contract_status', $contract_type_data);
					}


					$data = array(
						'department'     => "ES",
						'market_segment' => $market_segment,
						'status'         => $contract_status_id,
					);

					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end))));
				} //else end here

			}
		} // foreach end here


	}



	public function contract_code()
	{
		$path = 'asset/decamCode.xlsx';
		$Reader = new SpreadsheetReader($path);
		//                echo "<pre>"; print_r( $Reader); die();
		$data = [];
		$count = 0;
		$new = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key >= 0) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[0];
				$contract_no            = $row[1];
				$contract_status        = $row[2];
				$company_name           = $row[10];
				$address1               = $row[5];
				$service_frequency      = $row[5];
				$contract_start         = $row[8];
				$contract_end           = $row[9];
				$code         = $row[6];

				//echo date('Y-m-d H:i:s', strtotime($contract_end));
				//die();

				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					//'contract_start' =>date('Y-m-d',strtotime($contract_start)),
					//'contract_end' => date('Y-m-d', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);


				if ($info == FALSE) {

					$new++;
					echo "<pre>New-" . $new . '</pre>';

					//die();


				} else { // update existing record
					//print_r($info);
					$addressval = $info->address2 . ' (' . $code . ')';
					$data = array(
						'address2'     => $addressval
					);
					echo "<pre>";
					echo $info->id . '-';
					print_r($addressval);
					echo "</pre>";
					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'address1' => $address1));
				} //else end here

			}
		} // foreach end here


	}

	public function decam_contract_code()
	{
		$tem_contract = $this->db->select('decam_contract2.*,customer.id as customer_id,customer.company_name as company_name,contract_status.id as contract_status_id')->join('contract_status', 'contract_status.name = decam_contract2.status', 'left')->join('customer', 'customer.account_no = decam_contract2.account_no', 'left')->where('flag', 4)->get('decam_contract2')->result();
		//   echo "<pre>"; print_r($tem_contract); die();
		$new = 0;
		foreach ($tem_contract as $dcon) {
			$contract_no = $dcon->contract_no;
			$address1 = $dcon->address1;
			$address2 = $dcon->address2;
			$code = $dcon->b_code;

			//if no company in system then need to add company first
			//     if($dcon->customer_id == ''){
			//         $custdata = array(
			//             'account_no'=>$dcon->account_no,
			//             'company_name'=>$dcon->customer_name
			//         );
			//         
			//         $this->db->insert('customer', $custdata);
			//	 $dcon->customer_id = $this->db->insert_id();
			//     }
			//     
			//     $contract_data = array(
			//                'contract_no'       => $contractNo,
			//                'contract_name'     => $contractName,
			//                'customer_id'       => $accountNo,
			//                'contract_type'     => $contractType,
			//                'active_status'     => $Status,  
			//                'address1'          => $siteAddress1,
			//                'address2'          => $siteAddress2,
			//                'travel_time'       => $travel_time,
			//                'postcode'          => $postCode,
			//                'market_segment'    => $marketSegment,
			//                'service_frequency' => $serviceFrequency,
			//                'contract_start'    => $startDate,
			//                'contract_end'      => $endDate,
			//                'support_type'      => $supportType,
			//                'department'      => $department,
			//                'remarks'      => $remarks
			//            );



			//echo "<pre>";print_r($where);echo "</pre>";
			//                                for flag 1
			//				$info = $this->db->select('*')->where('contract_no',$contract_no)->get('customer_contract')->row();
			//                                flag 3
			$info = $this->db->select('*')->where('contract_no', $contract_no)->where('address2', $address2)->get('customer_contract')->row();

			if (sizeof($info) == 0) {
				$this->db->where('id', $dcon->id)->set('flag', 0)->update('decam_contract2');
				$new++;
				echo "<pre>New-" . $new . '</pre>';

				//die();


			} else { // update existing record
				//print_r($info);
				$addressval = $info->address2 . ' (' . $code . ')';
				$data = array(
					'address2'     => $addressval
				);
				echo "<pre>";
				echo $info->id . '-';
				echo $info->contract_no;
				print_r($addressval);
				echo "</pre>";

				$this->db->where('id', $info->id)->set($data)->update('customer_contract');
			} //else end here
		}
	}



	public function fscustomer()
	{
		$path = 'asset/fs.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		foreach ($Reader as $key => $row) {
			// customer check
			if ($key > 0) {

				$account_no             = $row[0];
				$company_name           = $row[7];

				$where = array('account_no' => $account_no);
				$info = $this->main_model->existance('customer', $where);
				//$info1 = $this->main_model->existance('customer2', $where);

				//echo "<pre>";print_r($info);echo "</pre>";

				//if($info == FALSE && $info1 == FALSE){
				if ($info == FALSE) {
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);

					$this->main_model->add('customer', $data);
				}
			}
		}
	}


	public function fscustomerContract()
	{
		$path = 'asset/fs.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key > 0) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[0];
				$contract_no            = $row[5];
				$contract_status        = $row[1];
				$company_name           = $row[7];
				$address1               = $row[8];
				$service_frequency      = $row[12];
				$contract_start         = $row[3];
				$contract_end           = $row[4];
				$contract_type          = $row[11];
				//$system                 = $row[10];
				$support_type           = $row[10];
				// $contact_acc_manager    = $row[15];
				// $contact_customer       = $row[16];
				// $contact_customer_no    = $row[17];
				// $contact_service_eng    = $row[18];
				// $contact_customer_email = $row[19];

				$contact_acc_manager    = '';
				$contact_customer       = '';
				$contact_customer_no    = '';
				$contact_service_eng    = '';
				$contact_customer_email = '';
				//$market_segment         = $row[21];



				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
					'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);
				if ($info == FALSE) {

					//first check and retrive account no id
					//$customer = $this->db->select('*')->where('account_no',$account_no)->get('customer2')->row();
					$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
					if (sizeof($customer) > 0) {
						$customer_id = $customer[0]->id;
					} else {
						//$customer_id = 1; // will not applicable
					}


					//retrive contract_tupe id
					$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
					if (sizeof($contract_type_details) > 0) {
						$contract_type_id = $contract_type_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_type
						);
						$contract_type_id = $this->main_model->addAndGetID('contract_type', $contract_type_data);
					}


					//retrive support_type
					//$support_type = $val['Support type'];

					//$support_type_details = $this->db->select('*')->where('name',$support_type)->get('support_type')->row();
					$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
					if (sizeof($support_type_details) > 0) {
						$support_type_id = $support_type_details[0]->id;
					} else {
						$support_type_data = array(
							'name' => $support_type
						);
						$support_type_id = $this->common_model->addAndGetID('support_type', $support_type_data);
					}

					//retrive post code
					//$address = $val['Customer Account No'];
					$postcode = (int) substr($address1, -7);
					$postcode = (int) $postcode;
					if ($postcode == 0) {
						$postcode = '';
					}


					// contract_status
					$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					if (sizeof($contract_status_details) > 0) {
						$contract_status_id = $contract_status_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_status
						);
						$contract_status_id = $this->main_model->addAndGetID('contract_status', $contract_type_data);
					}

					// add market segment to database table
					// $market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
					// if(($market_segment_check) == FALSE){
					//     $data = array(
					//         'name'=>$market_segment
					//     );
					//     $this->main_model->add('contract_market_segment',$data);
					// }



					$customer_contract_data = array(
						'contract_no'       => $contract_no,
						'contract_name'     => $contract_no,
						'customer_id'       => $customer_id,
						'contract_type'     => $contract_type_id,
						'support_type'      => $support_type_id,
						'address1'          => $address1,
						'address2'          => 'duplicate',
						'postcode'          => $postcode,
						'department'        => "FS",
						'contract_start'    => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end'      => date('Y-m-d H:i:s', strtotime($contract_end)),
						//'market_segment'    =>$market_segment,
						//'service_frequency' =>$service_frequency, 
						'status'            => $contract_status_id,
						'create_by'         => 1,
						'create_date'       => date('Y-m-d H:i:s'),

					);



					//now check duplicate and insert in database
					$contract_id = $this->main_model->addAndGetID('customer_contract', $customer_contract_data);

					//now save contract system

					//$system_details = $this->db->select('*')->where('system_name',$system)->get('system')->row();
					// $system_details = $this->main_model->read_table('system', array('system_name' => $system));

					// if(sizeof($system_details) > 0){
					//     $system_id = $system_details[0]->id;
					// }else{
					//     $system_data = array(
					//         'system_name'=>$system
					//     );
					//     $system_id = $this->main_model->addAndGetID('system',$system_data);
					// }

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       => $contract_id,
						//'system_id'         =>$system_id,
						'customer_id'       => $customer_id,
						'service_frequency' => $service_frequency,
					);

					$contract_system_id = $this->main_model->addAndGetID('contract_system', $contract_system_data);

					//then import contract contact

					//$contact_no = $val['CONTACT NUMBER'];

					if ($contact_acc_manager != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 2,
							'contact_name'    => $contact_acc_manager,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}
					$contact_supervisor = '';
					if ($contact_supervisor != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 1,
							'contact_name'    => $contact_supervisor,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}


					if ($contact_customer != '') {
						$save_data = array(
							'contract_id'   => $contract_id,
							'contact_type'  => 5,
							'contact_name'  => $contact_customer,
							'contact_no'    => $contact_customer_no,
							'contact_email' => $contact_customer_email,
							'creator_id'    => 1,
							'create_date'   => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					if ($contact_service_eng != '') {
						$save_data = array(
							'contract_id' => $contract_id,
							'contact_type' => 3,
							'contact_name' => $contact_service_eng,
							//'contact_no'=>$contact_no,
							//'contact_email'=>$contact_email,
							'creator_id' => 1,
							'create_date' => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					//  customer contract import end

				} else { // update existing record

					$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					if (sizeof($contract_status_details) > 0) {
						$contract_status_id = $contract_status_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_status
						);
						$contract_status_id = $this->main_model->addAndGetID('contract_status', $contract_type_data);
					}


					$data = array(
						'department'     => "FS",
						'market_segment' => $market_segment,
						'status'         => $contract_status_id,
					);

					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end))));
				} //else end here

			}
		} // foreach end here


	}


	public function fscustomer2()
	{
		$path = 'asset/customer-listing-fs.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		foreach ($Reader as $key => $row) {
			// customer check
			if ($key > 0) {

				$account_no             = $row[1];
				$company_name           = $row[10];

				$where = array('account_no' => $account_no);
				$info = $this->main_model->existance('customer', $where);
				//$info1 = $this->main_model->existance('customer2', $where);

				//echo "<pre>";print_r($info);echo "</pre>";

				//if($info == FALSE && $info1 == FALSE){
				if ($info == FALSE) {
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);

					$this->main_model->add('customer', $data);
				}
			}
		}
	}


	public function fscustomerContract2()
	{
		$path = 'asset/customer-listing-fs.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key > 0) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[1];
				$contract_no            = $row[0];
				//$contract_status        = $row[1];
				$company_name           = $row[10];
				$address1               = $row[16];
				$service_frequency      = $row[14];
				$contract_start         = $row[21];
				$contract_end           = $row[22];
				$contract_type          = $row[24];
				$system                 = $row[5];
				//$support_type           = $row[10];
				// $contact_acc_manager    = $row[15];
				// $contact_customer       = $row[16];
				// $contact_customer_no    = $row[17];
				// $contact_service_eng    = $row[18];
				// $contact_customer_email = $row[19];

				$contact_acc_manager    = '';
				$contact_customer       = '';
				$contact_customer_no    = '';
				$contact_service_eng    = '';
				$contact_customer_email = '';
				$market_segment         = $row[2];

				$zbf_no = $row[4];



				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
					'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);
				if ($info == FALSE) {

					//first check and retrive account no id
					//$customer = $this->db->select('*')->where('account_no',$account_no)->get('customer2')->row();
					$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
					if (sizeof($customer) > 0) {
						$customer_id = $customer[0]->id;
					} else {
						//$customer_id = 1; // will not applicable
					}


					//retrive contract_tupe id
					$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
					if (sizeof($contract_type_details) > 0) {
						$contract_type_id = $contract_type_details[0]->id;
					} else {
						$contract_type_data = array(
							'name' => $contract_type
						);
						$contract_type_id = $this->main_model->addAndGetID('contract_type', $contract_type_data);
					}


					//retrive support_type
					//$support_type = $val['Support type'];

					//$support_type_details = $this->db->select('*')->where('name',$support_type)->get('support_type')->row();
					/*$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
				    if(sizeof($support_type_details) > 0){
				        $support_type_id = $support_type_details[0]->id;
				    }else{
				        $support_type_data = array(
				            'name'=>$support_type
				        );
				        $support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);
				      
				    }*/

					//retrive post code
					//$address = $val['Customer Account No'];
					$postcode = (int) substr($address1, -7);
					$postcode = (int) $postcode;
					if ($postcode == 0) {
						$postcode = '';
					}


					// contract_status
					/*$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
				    if(sizeof($contract_status_details) > 0){
				        $contract_status_id = $contract_status_details[0]->id;
				    }else{
				        $contract_type_data = array(
				            'name'=>$contract_status
				        );
				        $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);
				      
				    }*/

					//add market segment to database table
					$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
					if (($market_segment_check) == FALSE) {
						$data = array(
							'name' => $market_segment
						);
						$this->main_model->add('contract_market_segment', $data);
					}



					$customer_contract_data = array(
						'contract_no'       => $contract_no,
						'zbf_no'            => $zbf_no,
						'contract_name'     => $contract_no,
						'customer_id'       => $customer_id,
						'contract_type'     => $contract_type_id,
						//'support_type'      =>$support_type_id,
						'address1'          => $address1,
						'address2'          => 'duplicate',
						'postcode'          => $postcode,
						'department'        => "FS",
						'contract_start'    => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end'      => date('Y-m-d H:i:s', strtotime($contract_end)),
						//'market_segment'    =>$market_segment,
						//'service_frequency' =>$service_frequency, 
						//'status'            =>$contract_status_id,
						'create_by'         => 1,
						'create_date'       => date('Y-m-d H:i:s'),

					);



					//now check duplicate and insert in database
					$contract_id = $this->main_model->addAndGetID('customer_contract', $customer_contract_data);

					//now save contract system
					$type = $row[6];
					$type2 = $row[7];
					if ($type != '') {
						$system_type = 1;
					} elseif ($type2 != '') {
						$system_type = 2;
					}
					$system_details = $this->main_model->read_table('system', array('system_name' => $system));

					if (sizeof($system_details) > 0) {
						$system_id = $system_details[0]->id;
					} else {
						$system_data = array(
							'system_name' => $system,
							'system_type' => $system_type
						);
						$system_id = $this->main_model->addAndGetID('system', $system_data);
					}

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       => $contract_id,
						'system_id'         => $system_id,
						'customer_id'       => $customer_id,
						'service_frequency' => $service_frequency,
					);

					$contract_system_id = $this->main_model->addAndGetID('contract_system', $contract_system_data);

					//then import contract contact

					//$contact_no = $val['CONTACT NUMBER'];

					if ($contact_acc_manager != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 2,
							'contact_name'    => $contact_acc_manager,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}
					$contact_supervisor = $row[11];
					if ($contact_supervisor != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 1,
							'contact_name'    => $contact_supervisor,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}


					if ($contact_customer != '') {
						$save_data = array(
							'contract_id'   => $contract_id,
							'contact_type'  => 5,
							'contact_name'  => $contact_customer,
							'contact_no'    => $contact_customer_no,
							'contact_email' => $contact_customer_email,
							'creator_id'    => 1,
							'create_date'   => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					if ($contact_service_eng != '') {
						$save_data = array(
							'contract_id' => $contract_id,
							'contact_type' => 3,
							'contact_name' => $contact_service_eng,
							//'contact_no'=>$contact_no,
							//'contact_email'=>$contact_email,
							'creator_id' => 1,
							'create_date' => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					//  customer contract import end

				} else { // update existing record

					//			    	$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					//			    	//$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					//			    	if(sizeof($contract_status_details) > 0){
					//			    	    $contract_status_id = $contract_status_details[0]->id;
					//			    	}else{
					//			    	    $contract_type_data = array(
					//			    	        'name'=>$contract_status
					//			    	    );
					//			    	    $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);
					//			    	  
					//			    	}

					$contract_status_id = 1;


					$data = array(
						'department'     => "FS",
						'zbf_no'         => $zbf_no,
						'market_segment' => $market_segment,
						'status'         => $contract_status_id,
					);

					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end))));
				} //else end here

			}
		} // foreach end here


	}




	public function fscustomer3()
	{
		$path = FCPATH . 'asset/decam.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		foreach ($Reader as $key => $row) {
			// customer check
			if ($key > 0) {

				$account_no             = $row[0];
				$company_name           = $row[15];

				$where = array('account_no' => $account_no);
				$info = $this->main_model->existance('customer', $where);
				//$info1 = $this->main_model->existance('customer2', $where);

				//echo "<pre>";print_r($info);echo "</pre>";

				//if($info == FALSE && $info1 == FALSE){
				if ($info == FALSE) {
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);

					$this->main_model->add('customer', $data);
				}
			}
		}
	}



	public function fscustomerContract3()
	{
		$path = 'asset/decam.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key > 0) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[0];
				$contract_no            = $row[11];
				$contract_status        = $row[2];
				$company_name           = $row[15];
				$address1               = $row[16];
				$address2               = $row[17];
				$service_frequency      = ''; //$row[14];
				$contract_start         = $row[9];
				$contract_end           = $row[10];
				$market_segment = 'OTHERS';
				//$contract_type          = $row[24];
				//$system                 = $row[5];
				//$support_type           = $row[10];
				// $contact_acc_manager    = $row[15];
				// $contact_customer       = $row[16];
				// $contact_customer_no    = $row[17];
				// $contact_service_eng    = $row[18];
				// $contact_customer_email = $row[19];

				$contact_acc_manager    = '';
				$contact_customer       = '';
				$contact_customer_no    = '';
				$contact_service_eng    = '';
				$contact_customer_email = '';
				//$market_segment         = $row[2];

				//$zbf_no = $row[4];



				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
					'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);
				if ($info == FALSE) {

					//first check and retrive account no id
					//$customer = $this->db->select('*')->where('account_no',$account_no)->get('customer2')->row();
					$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
					if (sizeof($customer) > 0) {
						$customer_id = $customer[0]->id;
					} else {
						//$customer_id = 1; // will not applicable
					}


					//retrive contract_tupe id
					/*$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
				    if(sizeof($contract_type_details) > 0){
				        $contract_type_id = $contract_type_details[0]->id;
				    }else{
				        $contract_type_data = array(
				            'name'=>$contract_type
				        );
				        $contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);
				      
				    }*/


					//retrive support_type
					//$support_type = $val['Support type'];

					//$support_type_details = $this->db->select('*')->where('name',$support_type)->get('support_type')->row();
					/*$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
				    if(sizeof($support_type_details) > 0){
				        $support_type_id = $support_type_details[0]->id;
				    }else{
				        $support_type_data = array(
				            'name'=>$support_type
				        );
				        $support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);
				      
				    }*/

					//retrive post code
					//$address = $val['Customer Account No'];
					$postcode = (int) substr($address1, -7);
					$postcode = (int) $postcode;
					if ($postcode == 0) {
						$postcode = '';
					}


					// contract_status
					/*$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
				    if(sizeof($contract_status_details) > 0){
				        $contract_status_id = $contract_status_details[0]->id;
				    }else{
				        $contract_type_data = array(
				            'name'=>$contract_status
				        );
				        $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);
				      
				    }*/

					//add market segment to database table
					/*$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
				    if(($market_segment_check) == FALSE){
				        $data = array(
				            'name'=>$market_segment
				        );
				        $this->main_model->add('contract_market_segment',$data);
				    }*/



					$customer_contract_data = array(
						'contract_no'       => $contract_no,
						//'zbf_no'            =>$zbf_no,
						'contract_name'     => $contract_no,
						'customer_id'       => $customer_id,
						//'contract_type'     =>$contract_type_id,
						//'support_type'      =>$support_type_id,
						'address1'          => $address1,
						'address2'          => $address2,
						'postcode'          => $postcode,
						'department'        => "DC",
						'contract_start'    => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end'      => date('Y-m-d H:i:s', strtotime($contract_end)),
						//'market_segment'    =>$market_segment,
						//'service_frequency' =>$service_frequency, 
						//'status'            =>$contract_status_id,
						'create_by'         => 1,
						'create_date'       => date('Y-m-d H:i:s'),

					);



					//now check duplicate and insert in database
					$contract_id = $this->main_model->addAndGetID('customer_contract', $customer_contract_data);

					//now save contract system
					/*$type = $row[6];
				    $type2 = $row[7];
				    if($tpye !=''){
				    	$system_type = 1;
				    }elseif($type2 != ''){
				    	$system_type = 2;
				    }
				    $system_details = $this->main_model->read_table('system', array('system_name' => $system));
				    
				    if(sizeof($system_details) > 0){
				        $system_id = $system_details[0]->id;
				    }else{
				        $system_data = array(
				            'system_name'=>$system_type,
				            'system_type'=>$system
				        );
				        $system_id = $this->main_model->addAndGetID('system',$system_data);
				    }*/

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       => $contract_id,
						//'system_id'         =>$system_id,
						'customer_id'       => $customer_id,
						//'service_frequency' =>$service_frequency,
					);

					$contract_system_id = $this->main_model->addAndGetID('contract_system', $contract_system_data);

					//then import contract contact

					//$contact_no = $val['CONTACT NUMBER'];

					if ($contact_acc_manager != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 2,
							'contact_name'    => $contact_acc_manager,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}
					$contact_supervisor = '';
					if ($contact_supervisor != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 1,
							'contact_name'    => $contact_supervisor,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}


					if ($contact_customer != '') {
						$save_data = array(
							'contract_id'   => $contract_id,
							'contact_type'  => 5,
							'contact_name'  => $contact_customer,
							'contact_no'    => $contact_customer_no,
							'contact_email' => $contact_customer_email,
							'creator_id'    => 1,
							'create_date'   => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					if ($contact_service_eng != '') {
						$save_data = array(
							'contract_id' => $contract_id,
							'contact_type' => 3,
							'contact_name' => $contact_service_eng,
							//'contact_no'=>$contact_no,
							//'contact_email'=>$contact_email,
							'creator_id' => 1,
							'create_date' => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					//  customer contract import end

				} else { // update existing record

					// $contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					// //$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					// if(sizeof($contract_status_details) > 0){
					//     $contract_status_id = $contract_status_details[0]->id;
					// }else{
					//     $contract_type_data = array(
					//         'name'=>$contract_status
					//     );
					//     $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);

					// }


					$data = array(
						'department'     => "DC",
						'address2'          => $address2,
						//'zbf_no'         =>$zbf_no,
						'market_segment' => $market_segment,
						//'status'         =>$contract_status_id,
					);

					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end))));
				} //else end here

			}
		} // foreach end here


	}

	function newcust()
	{
		$path = FCPATH . 'asset/newcust.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		foreach ($Reader as $key => $row) {
			// customer check
			if ($key > 0) {

				$account_no             = $row[0];
				$company_name           = $row[3];

				$where = array('account_no' => $account_no);
				$info = $this->main_model->existance('customer', $where);
				//$info1 = $this->main_model->existance('customer2', $where);

				//echo "<pre>";print_r($info);echo "</pre>";

				//if($info == FALSE && $info1 == FALSE){
				if ($info == FALSE) {
					$data = array(
						'account_no' => $account_no,
						'company_name' => $company_name,
					);

					$this->main_model->add('customer', $data);
				}
			}
		}
	}



	public function newcustcontract()
	{
		$path = 'asset/newcust.xlsx';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			// contract check
			if ($key > 0) {



				////////////////////////////////////////
				// customer contact start import here //
				////////////////////////////////////////

				// assign cell value to variable
				$account_no             = $row[0];
				$contract_no            = $row[1];
				$contract_status        = 1;
				$company_name           = $row[3];
				$address1               = $row[4];
				$address2               = '';
				$service_frequency      = $row[5];
				$contract_start         = $row[6];
				$contract_end           = $row[7];
				$market_segment = 'OTHERS';
				//$contract_type          = $row[24];
				//$system                 = $row[5];
				//$support_type           = $row[10];
				// $contact_acc_manager    = $row[15];
				// $contact_customer       = $row[16];
				// $contact_customer_no    = $row[17];
				// $contact_service_eng    = $row[18];
				// $contact_customer_email = $row[19];

				$contact_acc_manager    = '';
				$contact_customer       = '';
				$contact_customer_no    = '';
				$contact_service_eng    = '';
				$contact_customer_email = '';
				//$market_segment         = $row[2];

				//$zbf_no = $row[4];



				$where = array(
					'contract_no' => $contract_no,
					'address1' => $address1,
					'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
					'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
				);
				//echo "<pre>";print_r($where);echo "</pre>";

				$info = $this->main_model->existance('customer_contract', $where);
				if ($info == FALSE) {

					//first check and retrive account no id
					//$customer = $this->db->select('*')->where('account_no',$account_no)->get('customer2')->row();
					$customer = $this->main_model->read_table('customer', array('account_no' => $account_no));
					if (sizeof($customer) > 0) {
						$customer_id = $customer[0]->id;
					} else {
						//$customer_id = 1; // will not applicable
					}


					//retrive contract_tupe id
					/*$contract_type_details = $this->main_model->read_table('contract_type', array('name' => $contract_type));
				    if(sizeof($contract_type_details) > 0){
				        $contract_type_id = $contract_type_details[0]->id;
				    }else{
				        $contract_type_data = array(
				            'name'=>$contract_type
				        );
				        $contract_type_id = $this->main_model->addAndGetID('contract_type',$contract_type_data);
				      
				    }*/


					//retrive support_type
					//$support_type = $val['Support type'];

					//$support_type_details = $this->db->select('*')->where('name',$support_type)->get('support_type')->row();
					/*$support_type_details = $this->main_model->read_table('support_type', array('name' => $support_type));
				    if(sizeof($support_type_details) > 0){
				        $support_type_id = $support_type_details[0]->id;
				    }else{
				        $support_type_data = array(
				            'name'=>$support_type
				        );
				        $support_type_id = $this->common_model->addAndGetID('support_type',$support_type_data);
				      
				    }*/

					//retrive post code
					//$address = $val['Customer Account No'];
					$postcode = (int) substr($address1, -7);
					$postcode = (int) $postcode;
					if ($postcode == 0) {
						$postcode = '';
					}


					// contract_status
					/*$contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
				    if(sizeof($contract_status_details) > 0){
				        $contract_status_id = $contract_status_details[0]->id;
				    }else{
				        $contract_type_data = array(
				            'name'=>$contract_status
				        );
				        $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);
				      
				    }*/

					//add market segment to database table
					/*$market_segment_check = $this->main_model->existance('contract_market_segment', array('name' => $market_segment));
				    if(($market_segment_check) == FALSE){
				        $data = array(
				            'name'=>$market_segment
				        );
				        $this->main_model->add('contract_market_segment',$data);
				    }*/



					$customer_contract_data = array(
						'contract_no'       => $contract_no,
						//'zbf_no'            =>$zbf_no,
						'contract_name'     => $contract_no,
						'customer_id'       => $customer_id,
						//'contract_type'     =>$contract_type_id,
						//'support_type'      =>$support_type_id,
						'address1'          => $address1,
						'address2'          => $address2,
						'postcode'          => $postcode,
						'department'        => "ES1",
						'contract_start'    => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end'      => date('Y-m-d H:i:s', strtotime($contract_end)),
						//'market_segment'    =>$market_segment,
						//'service_frequency' =>$service_frequency, 
						//'status'            =>$contract_status_id,
						'create_by'         => 1,
						'create_date'       => date('Y-m-d H:i:s'),

					);



					//now check duplicate and insert in database
					$contract_id = $this->main_model->addAndGetID('customer_contract', $customer_contract_data);

					//now save contract system
					/*$type = $row[6];
				    $type2 = $row[7];
				    if($tpye !=''){
				    	$system_type = 1;
				    }elseif($type2 != ''){
				    	$system_type = 2;
				    }
				    $system_details = $this->main_model->read_table('system', array('system_name' => $system));
				    
				    if(sizeof($system_details) > 0){
				        $system_id = $system_details[0]->id;
				    }else{
				        $system_data = array(
				            'system_name'=>$system_type,
				            'system_type'=>$system
				        );
				        $system_id = $this->main_model->addAndGetID('system',$system_data);
				    }*/

					//now relate contract with system
					$contract_system_data = array(
						'contract_id'       => $contract_id,
						//'system_id'         =>$system_id,
						'customer_id'       => $customer_id,
						//'service_frequency' =>$service_frequency,
					);

					$contract_system_id = $this->main_model->addAndGetID('contract_system', $contract_system_data);

					//then import contract contact

					//$contact_no = $val['CONTACT NUMBER'];

					if ($contact_acc_manager != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 2,
							'contact_name'    => $contact_acc_manager,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}
					$contact_supervisor = '';
					if ($contact_supervisor != '') {
						$save_data = array(
							'contract_id'     => $contract_id,
							'contact_type'    => 1,
							'contact_name'    => $contact_supervisor,
							//'contact_no'    =>$contact_no,
							//'contact_email' =>$contact_email,
							'creator_id'      => 1,
							'create_date'     => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}


					if ($contact_customer != '') {
						$save_data = array(
							'contract_id'   => $contract_id,
							'contact_type'  => 5,
							'contact_name'  => $contact_customer,
							'contact_no'    => $contact_customer_no,
							'contact_email' => $contact_customer_email,
							'creator_id'    => 1,
							'create_date'   => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					if ($contact_service_eng != '') {
						$save_data = array(
							'contract_id' => $contract_id,
							'contact_type' => 3,
							'contact_name' => $contact_service_eng,
							//'contact_no'=>$contact_no,
							//'contact_email'=>$contact_email,
							'creator_id' => 1,
							'create_date' => date('Y-m-d H:i:s')
						);

						$this->main_model->add('contract_contact', $save_data);
					}

					//  customer contract import end

				} else { // update existing record

					// $contract_status_details = $this->main_model->read_table('contract_status', array('name' => $contract_status));
					// //$contract_type_details = $this->db->select('*')->where('name',$contract_status)->get('contract_status')->row();
					// if(sizeof($contract_status_details) > 0){
					//     $contract_status_id = $contract_status_details[0]->id;
					// }else{
					//     $contract_type_data = array(
					//         'name'=>$contract_status
					//     );
					//     $contract_status_id = $this->main_model->addAndGetID('contract_status',$contract_type_data);

					// }


					$data = array(
						'department'     => "ES1",
						'address2'          => $address2,
						//'zbf_no'         =>$zbf_no,
						'market_segment' => $market_segment,
						//'status'         =>$contract_status_id,
					);

					$this->main_model->update('customer_contract', $data, array('contract_no' => $contract_no, 'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end))));
				} //else end here

			}
		} // foreach end here


	}



	function customer()
	{

		foreach ($csv_array as $val) {
			//                        customer import start
			//                        first import data in customer table
			$customer_data = array(
				'account_no' => $val['Customer Account No'],
				'company_name' => $val['Customer Name'],
				'entry_date' => date('Y-m-d H:i:s')
			);
			$customer_dataa[] = $customer_data;
			// $customer_status = $val['Customer Status'];
			//if($customer_status != 'TERMINATED' && $customer_status != 'EXPIRED'){
			//now check duplicate and insert in database
			$duplicate = $this->db->select('*')->where('account_no', $val['Customer Account No'])->get('customer')->result();
			if (sizeof($duplicate) > 0) {
				$resp['count']['duplicate']++;
			} else {
				$this->common_model->save('customer', $customer_data);
			}
		}
	}



	function customer_contract()
	{

		foreach ($csv_array as $val) {

			//  // customer contract import start
			$account_no = $val['Customer Account No'];
			//first check and retrive account no id
			$customer = $this->db->select('*')->where('account_no', $account_no)->get('customer')->row();
			if (sizeof($customer) > 0) {
				$customer_id = $customer->id;
			} else {
				//$customer_id = 1; // will not applicable
			}

			//retrive contract_tupe id
			$contract_type = $val['Contract Type'];
			$contract_type_details = $this->db->select('*')->where('name', $contract_type)->get('contract_type')->row();
			if (sizeof($contract_type_details) > 0) {
				$contract_type_id = $contract_type_details->id;
			} else {
				$contract_type_data = array(
					'name' => $contract_type
				);
				$contract_type_id = $this->common_model->save('contract_type', $contract_type_data);
			}

			//retrive support_type
			$support_type = $val['Support type'];
			$support_type_details = $this->db->select('*')->where('name', $support_type)->get('support_type')->row();
			if (sizeof($support_type_details) > 0) {
				$support_type_id = $support_type_details->id;
			} else {
				$support_type_data = array(
					'name' => $support_type
				);
				$support_type_id = $this->common_model->save('support_type', $support_type_data);
			}

			//                        retrive post code
			//$address = $val['Customer Account No'];
			$postcode = (int) substr($val['SITE ADDRESS'], -7);
			$postcode = (int) $postcode;
			if ($postcode == 0) {
				$postcode = '';
			}
			//retrive market segment
			//                        $market_segment = $val['Market Segment'];

			$market_segment = 'OTHERS';

			//retrive service frequency
			$service_frequency = $val['SERVICING FREQUENCY'];

			$contract_no = $val['Customer Contract No'];
			//need to check duplicate or not
			$contract_no_details = $this->db->select('*')->where('contract_no', $contract_no)->or_where('contract_name', $contract_no)->get('customer_contract')->result();
			if (sizeof($contract_no_details) > 0) {
				$customer_contract_data = array(
					'contract_no' => $val['Customer Contract No'],
					'contract_name' => $val['Customer Contract No'],
					'customer_id' => $customer_id,
					'contract_type' => $contract_type_id,
					'support_type' => $support_type_id,
					'address1' => $val['SITE ADDRESS'],
					'address2' => 'duplicate',
					'postcode' => $postcode,
					'contract_start' => date('Y-m-d H:i:s', strtotime($val['CONTRACT START DATE'])),
					'contract_end' => date('Y-m-d H:i:s', strtotime($val['Contract End Date'])),
					'market_segment' => $market_segment,
					'service_frequency' => $service_frequency,
					'status' => 1,
					'create_by' => 1,
					'create_date' => date('Y-m-d H:i:s'),

				);
			} else {
				$customer_contract_data = array(
					'contract_no' => $val['Customer Contract No'],
					'contract_name' => $val['Customer Contract No'],
					'customer_id' => $customer_id,
					'contract_type' => $contract_type_id,
					'support_type' => $support_type_id,
					'address1' => $val['SITE ADDRESS'],
					'postcode' => $postcode,
					'contract_start' => date('Y-m-d H:i:s', strtotime($val['CONTRACT START DATE'])),
					'contract_end' => date('Y-m-d H:i:s', strtotime($val['Contract End Date'])),
					'market_segment' => $market_segment,
					'service_frequency' => $service_frequency,
					'status' => 1,
					'create_by' => 1,
					'create_date' => date('Y-m-d H:i:s'),

				);
			}
			//                         print_r($customer_contract_data);
			//now check duplicate and insert in database
			$contract_id = $this->common_model->save('customer_contract', $customer_contract_data);

			//now save contract system
			$system = $val['System Name'];
			$system_details = $this->db->select('*')->where('system_name', $system)->get('system')->row();

			if (sizeof($system_details) > 0) {
				$system_id = $system_details->id;
			} else {
				$system_data = array(
					'system_name' => $system
				);
				$system_id = $this->common_model->save('system', $system_data);
			}
			//now relate contract with system
			$contract_system_data = array(
				'contract_id' => $contract_id,
				'system_id' => $system_id,
				'customer_id' => $customer_id,
				'service_frequency' => $service_frequency,
			);

			$contract_system_id = $this->common_model->save('contract_system', $contract_system_data);

			//then import contract contact
			$contact_acc_manager = $val['Contact NEW ACCOUNT MANAGER'];
			$contact_supervisor = $val['CONTACT Supervisor'];
			$contact_service_eng = $val['Contact Service Engineer'];

			$contact_no = $val['CONTACT NUMBER'];
			$contact_email = $val['Contact Email'];

			if ($contact_acc_manager != '') {
				$save_data = array(
					'contract_id' => $contract_id,
					'contact_type' => 2,
					'contact_name' => $contact_acc_manager,
					'contact_no' => $contact_no,
					'contact_email' => $contact_email,
					'creator_id' => 1,
					'create_date' => date('Y-m-d H:i:s')
				);

				$this->common_model->save('contract_contact', $save_data);
			}

			if ($contact_supervisor != '') {
				$save_data = array(
					'contract_id' => $contract_id,
					'contact_type' => 1,
					'contact_name' => $contact_supervisor,
					'contact_no' => $contact_no,
					'contact_email' => $contact_email,
					'creator_id' => 1,
					'create_date' => date('Y-m-d H:i:s')
				);

				$this->common_model->save('contract_contact', $save_data);
			}

			if ($contact_service_eng != '') {
				$save_data = array(
					'contract_id' => $contract_id,
					'contact_type' => 3,
					'contact_name' => $contact_service_eng,
					'contact_no' => $contact_no,
					'contact_email' => $contact_email,
					'creator_id' => 1,
					'create_date' => date('Y-m-d H:i:s')
				);

				$this->common_model->save('contract_contact', $save_data);
			}

			//  customer contract import end

		}
	}


	public function fscontract()
	{

		$this->load->model('common_model');
		$path = 'asset/fscustomer.xlsm';
		$Reader = new SpreadsheetReader($path);
		$data = [];
		$count = 0;
		foreach ($Reader as $key => $row) {
			echo "<pre>";
			// print_r($row);
			// contract check
			if ($key > 0) {
				$count++;
				if ($row[2] != '') {

					// assign cell value to variable
					$account_no             = '202200' . $count;
					$contract_no            = '220000' . $count;
					$zbf_no = $row[1];
					$contract_status        = 9;
					$company_name           = $row[2];
					$address1               = $row[3];
					$address2               = '';
					$service_frequency      = $row[6];
					$contract_start         = $row[4];
					$contract_end           = $row[5];

					//retrive contract type id
					if ($row[7] == 'Non-Comprehensive') {
						$contract_type = 315;
					} else if ($row[7] == 'Comprehensive') {
						$contract_type = 317;
					}
					// retrive support type id
					$support_type = 4;

					$market_segment = $row[8];



					// first need to check exist customer or not
					// if not then add and generate customer id

					// if yes then use exist customer id

					$customer = $this->db->select('*')->where('company_name', $company_name)->get('customer')->row();
					if (sizeof($customer) > 0) {
						$customer_id = $customer->id;
						echo 'exist customer-';
					} else {
						$customer_data = array(
							'account_no' => $account_no,
							'company_name' => $company_name,
							'entry_date' => date('Y-m-d H:i:s')
						);
						echo "New customer-";
						$customer_id = $this->common_model->save('customer', $customer_data);
					}

					//now store customer contract data
					$customer_contract_data = array(
						'contract_no' => $contract_no,
						'zbf_no' => $zbf_no,
						'customer_id' => $customer_id,
						'contract_type' => $contract_type,
						'support_type' => $support_type,
						'address1' => $address1,
						// 'postcode' => $postcode,
						'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
						'market_segment' => $market_segment,
						'service_frequency' => $service_frequency,
						'department' => 'FS',
						'status' => 1,
						'create_by' => 1,
						'create_date' => date('Y-m-d H:i:s'),

					);

					print_r($customer_contract_data);

					//now check duplicate and insert in database
					$where = array(
						'contract_no' => $contract_no,
						'address1' => $address1,
						'contract_start' => date('Y-m-d H:i:s', strtotime($contract_start)),
						'contract_end' => date('Y-m-d H:i:s', strtotime($contract_end)),
					);
					//echo "<pre>";print_r($where);echo "</pre>";

					$info = $this->main_model->existance('customer_contract', $where);
					if ($info == FALSE) {
						$contract_id = $this->common_model->save('customer_contract', $customer_contract_data);
					}

					// 
				}
			}
		}
	}
}
