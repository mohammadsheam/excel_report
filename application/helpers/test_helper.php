<?php

function getId($table,$where=array()){
	$CI = & get_instance();
    $CI->db->select('id');
    $CI->db->from($table);
    $CI->db->where($where);
    $tbl = $CI->db->get()->row();
    if($tbl){
    	return $tbl->id;
    }else{
    	return 0;
    }
}